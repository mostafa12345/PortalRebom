/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "ORDERPRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orderproduct.findAll", query = "SELECT o FROM Orderproduct o")
    , @NamedQuery(name = "Orderproduct.findById", query = "SELECT o FROM Orderproduct o WHERE o.id = :id")
    , @NamedQuery(name = "Orderproduct.findByStatus", query = "SELECT o FROM Orderproduct o WHERE o.status = :status")
    , @NamedQuery(name = "Orderproduct.findByUpdateat", query = "SELECT o FROM Orderproduct o WHERE o.updateat = :updateat")
    , @NamedQuery(name = "Orderproduct.findByInsertdate", query = "SELECT o FROM Orderproduct o WHERE o.insertdate = :insertdate")
    , @NamedQuery(name = "Orderproduct.findByDeliverydate", query = "SELECT o FROM Orderproduct o WHERE o.deliverydate = :deliverydate")
    , @NamedQuery(name = "Orderproduct.findByDiscount", query = "SELECT o FROM Orderproduct o WHERE o.discount = :discount")
    , @NamedQuery(name = "Orderproduct.findByMeta", query = "SELECT o FROM Orderproduct o WHERE o.meta = :meta")
    , @NamedQuery(name = "Orderproduct.findByDeliverytype", query = "SELECT o FROM Orderproduct o WHERE o.deliverytype = :deliverytype")
    , @NamedQuery(name = "Orderproduct.findByPaymenttype", query = "SELECT o FROM Orderproduct o WHERE o.paymenttype = :paymenttype")})
public class Orderproduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "UPDATEAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateat;
    @Column(name = "INSERTDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertdate;
    @Column(name = "DELIVERYDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliverydate;
    @Column(name = "DISCOUNT")
    private Integer discount;
    @Size(max = 1024)
    @Column(name = "META")
    private String meta;
    @Column(name = "DELIVERYTYPE")
    private Integer deliverytype;
    @Column(name = "PAYMENTTYPE")
    private Integer paymenttype;
    @JoinColumn(name = "EMPLOYEE", referencedColumnName = "ID")
    @ManyToOne
    private Employee employee;
    @JoinColumn(name = "MARKETPLACE", referencedColumnName = "ID")
    @ManyToOne
    private Marketplace marketplace;
    @OneToMany(mappedBy = "orderproduct")
    private Collection<Orderitems> orderitemsCollection;

    public Orderproduct() {
    }

    public Orderproduct(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Date updateat) {
        this.updateat = updateat;
    }

    public Date getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(Date insertdate) {
        this.insertdate = insertdate;
    }

    public Date getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(Date deliverydate) {
        this.deliverydate = deliverydate;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Integer getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(Integer deliverytype) {
        this.deliverytype = deliverytype;
    }

    public Integer getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(Integer paymenttype) {
        this.paymenttype = paymenttype;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Marketplace getMarketplace() {
        return marketplace;
    }

    public void setMarketplace(Marketplace marketplace) {
        this.marketplace = marketplace;
    }

    @XmlTransient
    public Collection<Orderitems> getOrderitemsCollection() {
        return orderitemsCollection;
    }

    public void setOrderitemsCollection(Collection<Orderitems> orderitemsCollection) {
        this.orderitemsCollection = orderitemsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orderproduct)) {
            return false;
        }
        Orderproduct other = (Orderproduct) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.Orderproduct[ id=" + id + " ]";
    }
    
}
