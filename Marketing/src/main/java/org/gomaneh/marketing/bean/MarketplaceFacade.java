/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.model.Marketplace;

/**
 *
 * @author davood
 */
@Stateless
public class MarketplaceFacade extends AbstractFacade<Marketplace> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MarketplaceFacade() {
        super(Marketplace.class);
    }

    public List<Marketplace> findByPositions(boolean full, String lat, String lng, Employee em, int km) {
        if (full) {
            return this.em.createNativeQuery(String.format("SELECT * FROM `MARKETPLACE` WHERE ( 6378.137 * acos( cos( radians(35.7131529) ) * cos( radians( LATITUDE ) ) * cos( radians( LONGITUDE ) - radians(51.420514399999995) ) + sin( radians(35.7131529) ) * sin( radians( LATITUDE ) ) )) <= %s", km), Marketplace.class).getResultList();
        } else {
            return this.em.createQuery("SELECT m FROM Marketplace m WHERE m.employee= :employee").setParameter("employee", em).getResultList();
        }
    }

    public Marketplace findMarketByPositions(String lat, String lng) {
        return (Marketplace) this.em.createNativeQuery(String.format("SELECT * FROM `MARKETPLACE` WHERE ( 6378137 * acos( cos( radians(%s) ) * cos( radians( LATITUDE ) ) * cos( radians( LONGITUDE ) - radians(%s) ) + sin( radians(%s) ) * sin( radians( LATITUDE ) ) )) <= 5", lat, lng, lat), Marketplace.class).setMaxResults(1).getSingleResult();
    }
}
