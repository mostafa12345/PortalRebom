/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import org.gomaneh.marketing.bean.CartexFacade;
import org.gomaneh.marketing.bean.FinancialYearsFacade;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.model.Cartex;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.model.FinancialYears;
import org.gomaneh.marketing.model.Inventory;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Ajax;
import org.primefaces.context.RequestContext;

/**
 *
 * @author davood
 */
@Named(value = "settingInventoryController")
@ViewScoped
public class SettingInventoryController implements Serializable {

    @EJB
    private FinancialYearsFacade ejbFacade;
    @EJB
    private org.gomaneh.marketing.bean.ProductFacade productFacade;
    @EJB
    private org.gomaneh.marketing.bean.InventoryFacade inventoryFacade;
    @EJB
    private CartexFacade cartexFacade;
    private FinancialYears selected;
    private List<FinancialYears> items;
    private FinancialYears defaultSelected;
    private boolean transferInventory = true;
    private Employee currentUser;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
        defaultSelected = ejbFacade.getDefaultYears();
        if (defaultSelected == null) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, " باید سال مالی پیش فرض داشته باشید.", "اگر سال مالی وجود ندارد آن را ایجاد کرده و به صورت پیش فرض درآورید");
            RequestContext.getCurrentInstance().showMessageInDialog(fm);
        }
    }

    public void validateNumber(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
        String model = (String) value;

        if (!ejbFacade.validate(model)) {
            FacesMessage msg = new FacesMessage(
                    "سال مالی وارد شده معتبر نیست");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

    public boolean allowEdit(String years) {
        return ejbFacade.validateEdit(years);
    }

    public void prepareEdit(FinancialYears item) {
        if (allowEdit(item.getYears())) {
            selected = item;
            Ajax.oncomplete("$('.createModal').modal()");
        } else {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, " اجازه ویرایش یا حذف این سال کاری را ندارید.", "این سال کاری مورد استفاده قرار گرفته است");
            RequestContext.getCurrentInstance().showMessageInDialog(fm);
        }
    }

    public SettingInventoryController() {
    }

    public FinancialYears getSelected() {
        return selected;
    }

    public void setSelected(FinancialYears selected) {
        this.selected = selected;
    }

    public void prepareCreate() {
        selected = new FinancialYears();
    }

    public boolean firstYears() {
        return ejbFacade.count() == 0;
    }

    public FinancialYears getDefaultSelected() {
        return defaultSelected;
    }

    public void setDefaultSelected(FinancialYears defaultSelected) {
        this.defaultSelected = defaultSelected;
    }

    public void create() {
        if (selected.getId() == null) {
            selected.setEmployee(currentUser);
            selected.setStatus(1);
            ejbFacade.create(selected);
            setDefaultYears(selected);
            transfer();
            selected = null;
            JsfUtil.addSuccessMessage("سال کاری جدید افزوده شد.");
        } else {
            ejbFacade.edit(selected);
            JsfUtil.addSuccessMessage("سال کاری ویرایش شد.");
        }
    }

    private void transfer() {
        FinancialYears f = ejbFacade.findOpenInventory();
        f.setDoDate(new Date());
        f.setEmploeeDo(currentUser);
        f.setEnd(true);
        f.setStatus(0);
        ejbFacade.edit(f);
        if (f != null) {
            List<Cartex> vector = cartexFacade.findByYears(f.getYears());
            if (vector != null && !vector.isEmpty()) {
                for (Cartex obj : vector) {
                    Inventory inventory = new Inventory(obj.getProduct(),obj.getRemain(),
                            (transferInventory ? "بستن انبار و انتقال انبار به سال "+selected.getYears()
                                    : "بستن انبار سال جاری و صفر کردن انبار سال جدید."), new Date(),
                            currentUser, true, f.getYears(),null,(short)1);
                    inventory.setStatus((short)1);
                    inventoryFacade.create(inventory);
                    if (transferInventory) {
                        Inventory inventory2 = new Inventory(obj.getProduct(),obj.getRemain(),
                                "انتقال انبار از سال "+f.getYears(), new Date(),
                                currentUser, true, selected.getYears(),inventory.getId(),(short)0);
                        inventoryFacade.create(inventory2);
                    }
                }
            }
        }
    }

    public void destroy(FinancialYears item) {
        if (allowEdit(item.getYears())) {
            ejbFacade.remove(selected);
            selected = null;
            items = null;
            JsfUtil.addSuccessMessage("سال کاری حذف شد.");
        } else {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_WARN, " اجازه ویرایش یا حذف این سال کاری را ندارید.", "این سال کاری مورد استفاده قرار گرفته است");
            RequestContext.getCurrentInstance().showMessageInDialog(fm);
        }
    }

    public void setDefaultYears(FinancialYears years) {
        FinancialYears oldDefaultYears = ejbFacade.getDefaultYears();
        if (oldDefaultYears != null) {
            oldDefaultYears.setStatus(0);
            ejbFacade.edit(oldDefaultYears);
        }
        if (years == null && defaultSelected != null) {
            defaultSelected.setStatus(1);
            ejbFacade.edit(defaultSelected);
        } else {
            years.setStatus(1);
            ejbFacade.edit(years);
        }
        items = null;
        JsfUtil.addSuccessMessage("سال پیش فرض تغییر یافت");
    }

    public List<FinancialYears> getItems() {
        if (items == null) {
            items = getEjbFacade().findAll();
        }
        return items;
    }

    public FinancialYears getLastYears() {
        return getEjbFacade().getLastYears();
    }

    public void setItems(List<FinancialYears> items) {
        this.items = items;
    }

    public FinancialYearsFacade getEjbFacade() {
        return ejbFacade;
    }

    private FinancialYears getFinancialYears(Integer id) {
        return getEjbFacade().find(id);
    }

    public boolean isTransferInventory() {
        return transferInventory;
    }

    public void setTransferInventory(boolean transferInventory) {
        this.transferInventory = transferInventory;
    }

    @FacesConverter(forClass = FinancialYears.class)
    public static class SettingInventoryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SettingInventoryController controller = (SettingInventoryController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "settingInventoryController");
            return controller.getFinancialYears(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof FinancialYears) {
                FinancialYears o = (FinancialYears) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), FinancialYears.class.getName()});
                return null;
            }
        }

    }
}
