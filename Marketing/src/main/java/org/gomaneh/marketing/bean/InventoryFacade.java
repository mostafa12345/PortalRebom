/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.model.Inventory;
import org.gomaneh.marketing.model.Product;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

/**
 *
 * @author davood
 */
@Stateless
public class InventoryFacade extends AbstractFacade<Inventory> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InventoryFacade() {
        super(Inventory.class);
    }

    public List<Inventory> load(int first, int pageSize, List<SortMeta> multiSortMeta,
            int type, String years,
            Product productFilter, Integer numberFilter,
            Integer numberStateFilter, Date startFilter,
            Date endFilter, boolean dateStateFilter, Employee employeeFilter, Employee inventoryFilter) {
        Map<String, Object> filter = new HashMap<>();
        String sql = "select u from Inventory u where u.type = " + type + " and u.years=" + years;
        if (productFilter != null) {
            sql += " and u.product = :product ";
            filter.put("product", productFilter);
        }
        if (numberFilter != null && numberFilter > 0) {
            if (null != numberStateFilter) {
                switch (numberStateFilter) {
                    case 0:
                        sql += " and u.number = :number ";
                        break;
                    case 1:
                        sql += " and u.number >= :number ";
                        break;
                    case -1:
                        sql += " and u.number <= :number ";
                        break;
                }
            }
            filter.put("number", numberFilter);
        }
        if (dateStateFilter && startFilter != null) {
            sql += " and u.realDate = :realDate ";
            filter.put("realDate", startFilter);
        } else {
            if (startFilter != null && endFilter != null) {
                sql += " and ( u.realDate >= :start and  u.realDate <= :end  ) ";
                filter.put("start", startFilter);
                filter.put("end", endFilter);
            } else if (startFilter != null) {
                sql += " and u.realDate >= :start ";
                filter.put("start", startFilter);
            } else if (endFilter != null) {
                sql += " and u.realDate <= :end ";
                filter.put("end", endFilter);
            }
        }

        if (employeeFilter != null) {
            sql += " and u.receiver = :receiver ";
            filter.put("receiver", employeeFilter);
        }

        if (inventoryFilter != null) {
            sql += " and u.employee = :employee ";
            filter.put("employee", inventoryFilter);
        }
        if (multiSortMeta != null && !multiSortMeta.isEmpty()) {
            sql += " order by ";
            int i = 0;
            for (SortMeta f : multiSortMeta) {
                if (i > 0) {
                    sql += ",";
                }
                sql += "u." + f.getSortField() + " " + (f.getSortOrder().equals(SortOrder.ASCENDING)
                        ? "ASC"
                        : "DESC");
                i++;
            }
        } else {
            sql += " order by u.id DESC";
        }
        TypedQuery<Inventory> query = getEntityManager().createQuery(sql, Inventory.class).setFirstResult(first).setMaxResults(pageSize);
        if (!filter.isEmpty()) {
            filter.keySet().forEach((f) -> {
                query.setParameter(f, filter.get(f));
            });
        }
        return query.getResultList();
    }

    public int count(int first, int pageSize,
            int type, String years,
            Product productFilter, Integer numberFilter,
            Integer numberStateFilter, Date startFilter,
            Date endFilter, boolean dateStateFilter, Employee employeeFilter, Employee inventoryFilter) {
        Map<String, Object> filter = new HashMap<>();
        String sql = "select COUNT(u) from Inventory u where u.type = " + type + " and u.years=" + years;
        if (productFilter != null) {
            sql += " and u.product = :product ";
            filter.put("product", productFilter);
        }
        if (numberFilter != null && numberFilter > 0) {
            if (null != numberStateFilter) {
                switch (numberStateFilter) {
                    case 0:
                        sql += " and u.number = :number ";
                        break;
                    case 1:
                        sql += " and u.number >= :number ";
                        break;
                    case -1:
                        sql += " and u.number <= :number ";
                        break;
                }
            }
            filter.put("number", numberFilter);
        }
        if (dateStateFilter && startFilter != null) {
            sql += " and u.realDate = :realDate ";
            filter.put("realDate", startFilter);
        } else {
            if (startFilter != null && endFilter != null) {
                sql += " and ( u.realDate >= :start and  u.realDate <= :end  ) ";
                filter.put("start", startFilter);
                filter.put("end", endFilter);
            } else if (startFilter != null) {
                sql += " and u.realDate >= :start ";
                filter.put("start", startFilter);
            } else if (endFilter != null) {
                sql += " and u.realDate <= :end ";
                filter.put("end", endFilter);
            }
        }

        if (employeeFilter != null) {
            sql += " and u.receiver = :receiver ";
            filter.put("receiver", employeeFilter);
        }
        if (inventoryFilter != null) {
            sql += " and u.employee = :employee ";
            filter.put("employee", inventoryFilter);
        }
        TypedQuery<Long> query = getEntityManager().createQuery(sql, Long.class);
        if (!filter.isEmpty()) {
            filter.keySet().forEach((f) -> {
                query.setParameter(f, filter.get(f));
            });
        }
        return query.getSingleResult().intValue();
    }

}
