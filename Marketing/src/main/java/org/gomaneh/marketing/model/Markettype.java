/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "MARKETTYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Markettype.findAll", query = "SELECT m FROM Markettype m")
    , @NamedQuery(name = "Markettype.findById", query = "SELECT m FROM Markettype m WHERE m.id = :id")
    , @NamedQuery(name = "Markettype.findByName", query = "SELECT m FROM Markettype m WHERE m.name = :name")
    , @NamedQuery(name = "Markettype.findByStatus", query = "SELECT m FROM Markettype m WHERE m.status = :status")
    , @NamedQuery(name = "Markettype.findByMeta", query = "SELECT m FROM Markettype m WHERE m.meta = :meta")
    , @NamedQuery(name = "Markettype.findByUpdateat", query = "SELECT m FROM Markettype m WHERE m.updateat = :updateat")})
public class Markettype implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "NAME")
    private String name;
    @Column(name = "ICON")
    private String icon;
    @NotNull
    @Column(name = "STATUS")
    private int status = 1;
    @Size(max = 1024)
    @Column(name = "META")
    private String meta;
    @Column(name = "UPDATEAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateat;
    @JoinColumn(name = "EMPLOYEE", referencedColumnName = "ID")
    @ManyToOne
    private Employee employee;
    @OrderBy("priorty")
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "markrttype")
    private Collection<BaseattributeMarkrttype> baseattributeMarkrttypeCollection = new ArrayList<>();
    @Transient
    private Collection<BaseattributeMarkrttype> baseAttr = new ArrayList<>();
    @Transient
    private Collection<BaseattributeMarkrttype> placesAttr = new ArrayList<>();
    @Transient
    private Collection<BaseattributeMarkrttype> competitionAttr = new ArrayList<>();

    public Markettype() {
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Markettype(Integer id) {
        this.id = id;
    }

    public Markettype(Integer id, String name, int status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Date getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Date updateat) {
        this.updateat = updateat;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @XmlTransient
    public Collection<BaseattributeMarkrttype> getBaseattributeMarkrttypeCollection() {
        return baseattributeMarkrttypeCollection;
    }

    public void setBaseattributeMarkrttypeCollection(Collection<BaseattributeMarkrttype> baseattributeMarkrttypeCollection) {
        this.baseattributeMarkrttypeCollection = baseattributeMarkrttypeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Markettype)) {
            return false;
        }
        Markettype other = (Markettype) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.Markrttype[ id=" + id + " ]";
    }

    public Collection<BaseattributeMarkrttype> getBaseAttr() {
        if (baseAttr.isEmpty()) {
            baseAttr = new ArrayList<>();
            for (BaseattributeMarkrttype b : baseattributeMarkrttypeCollection) {
                if (b.getGroup() == 1) {
                    baseAttr.add(b);
                }
            }
        }
        return baseAttr;
    }

    public Collection<BaseattributeMarkrttype> getPlacesAttr() {
        if (placesAttr.isEmpty()) {
            placesAttr = new ArrayList<>();
            for (BaseattributeMarkrttype b : baseattributeMarkrttypeCollection) {
                if (b.getGroup() == 2) {
                    placesAttr.add(b);
                }
            }
        }
        return placesAttr;
    }

    public Collection<BaseattributeMarkrttype> getCompetitionAttr() {
        if (competitionAttr.isEmpty()) {
            competitionAttr = new ArrayList<>();
            for (BaseattributeMarkrttype b : baseattributeMarkrttypeCollection) {
                if (b.getGroup() == 3) {
                    competitionAttr.add(b);
                }
            }
        }
        return competitionAttr;
    }

    @PrePersist
    @PreUpdate
    public void preFlush() {
        updateat = new Date();
    }
}
