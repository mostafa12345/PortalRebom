/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Product;
import org.gomaneh.marketing.model.Productserials;

/**
 *
 * @author davood
 */
@Stateless
public class ProductserialsFacade extends AbstractFacade<Productserials> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductserialsFacade() {
        super(Productserials.class);
    }

    public List<Productserials> findByProduct(Product product) {
        return em.createQuery("SELECT s FROM Productserials s WHERE s.product = :product AND s.status = 1").setParameter("product", product).getResultList();
    }
    
}
