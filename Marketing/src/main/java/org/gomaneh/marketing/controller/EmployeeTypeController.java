package org.gomaneh.marketing.controller;

import org.gomaneh.marketing.model.EmployeeType;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.EmployeeTypeFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.gomaneh.marketing.model.Employee;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.context.RequestContext;

@Named("employeeTypeController")
@ViewScoped
public class EmployeeTypeController implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.EmployeeTypeFacade ejbFacade;
    private List<EmployeeType> items = null;
    private EmployeeType selected;
    private List<Grant> marktingRoles;
    private List<String> grantList;
    private Employee currentUser;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
    }

    public Employee getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Employee currentUser) {
        this.currentUser = currentUser;
    }

    public EmployeeTypeController() {
        marktingRoles = new ArrayList<>();
        marktingRoles.add(new Grant("MARKETING", "بازاریابی"));
        marktingRoles.add(new Grant("MARKETING_SELF_ADD", "افزودن در ناحیه تعریف شده"));
        marktingRoles.add(new Grant("MARKETING_SELF_SHOW", "مشاهده اطلاعات بازار تعریف شده توسط شخص"));
        marktingRoles.add(new Grant("MARKETING_SELF_EDIT", "ویرایش اطلاعات بازار تعریف شده توسط شخص"));
        marktingRoles.add(new Grant("MARKETING_SELF_REMOVE", "حذف اطلاعات بازار تعریف شده توسط شخص"));
        marktingRoles.add(new Grant("MARKETING_ALL_ADD", "افزودن در تمامی نقاط"));
        marktingRoles.add(new Grant("MARKETING_ALL_SHOW", "مشاهده اطلاعات تمامی نقاط"));
        marktingRoles.add(new Grant("MARKETING_ALL_EDIT", "ویرایش تمامی نقاط"));
        marktingRoles.add(new Grant("EMPLOYEE_SHOW", "کارمند"));
        marktingRoles.add(new Grant("EMPLOYEE_ADD", "افزودن کارمند"));
        marktingRoles.add(new Grant("EMPLOYEE_EDIT", "ویرایش اطلاعات کارمندان"));
        marktingRoles.add(new Grant("EMPLOYEE_CHANGE_PASSWORD", "تغییر رمز عبور کارمندان"));
        marktingRoles.add(new Grant("EMPLOYEE_CHANGE_ACCESS", "تغییر سطح دسترسی کارمندان"));
        marktingRoles.add(new Grant("EMPLOYEE_CHANGE_POSITION", "تغییر سمت کارمندان"));
        marktingRoles.add(new Grant("EMPLOYEE_EDIT_AREA", "ویرایش ناحیه جغرافیایی فعالیت کارمندان"));
        marktingRoles.add(new Grant("EMPLOYEE_REMOVE", "حذف کارمندان"));
        marktingRoles.add(new Grant("EMPLOYEETYPE_ACCESS", "ایجاو ویرایش سمت ها"));
        marktingRoles.add(new Grant("PRODUCT_CATEGORY", "تعریف و ویرایش دسته بندی محصولات"));
        marktingRoles.add(new Grant("PRODUCT_ADD", "افزودن محصولات"));
        marktingRoles.add(new Grant("PRODUCT_EDIT", "ویرایش محصولات"));
        marktingRoles.add(new Grant("PRODUCT_REMOVE", "حذف محصولات"));
        marktingRoles.add(new Grant("PRODUCT_ACCESS_BARCODE", "دسترسی به بخش بارکد محصولات"));
    }

    public EmployeeTypeFacade getEjbFacade() {
        return ejbFacade;
    }

    public List<String> getGrantList() {
        return grantList;
    }

    public void setGrantList(List<String> grantList) {
        this.grantList = grantList;
    }

    public void setEjbFacade(EmployeeTypeFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public List<Grant> getMarktingRoles() {
        return marktingRoles;
    }

    public void setMarktingRoles(List<Grant> marktingRoles) {
        this.marktingRoles = marktingRoles;
    }

    public EmployeeType getSelected() {
        return selected;
    }

    public void setSelected(EmployeeType selected) {
        this.selected = selected;
        if (selected != null) {
            grantList = new ArrayList<>();
            selected.getRoles().keySet().forEach((key) -> {
                grantList.add(key);
            });
        }
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EmployeeTypeFacade getFacade() {
        return ejbFacade;
    }

    public EmployeeType prepareCreate() {
        selected = new EmployeeType();
        initializeEmbeddableKey();
        grantList = null;
        return selected;
    }

    private String getGrantValue(String key) {
        for (Grant g : marktingRoles) {
            if (g.getKey() == null ? key == null : g.getKey().equals(key)) {
                return g.getValue();
            }
        }
        return null;
    }

    public void create() {
        if (currentUser.hasRole("EMPLOYEETYPE_ACCESS")) {
            selected.setRoles(new HashMap<>());
            grantList.forEach((s) -> {
                selected.getRoles().put(s, getGrantValue(s));
            });
            persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle2").getString("EmployeeTypeCreated"));
            if (!JsfUtil.isValidationFailed()) {
                items = null;    // Invalidate list of items to trigger re-query.
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void update() {
        if (currentUser.hasRole("EMPLOYEETYPE_ACCESS")) {
            selected.setRoles(new HashMap<>());
            grantList.forEach((s) -> {
                selected.getRoles().put(s, getGrantValue(s));
            });
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle2").getString("EmployeeTypeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void destroy() {
        if (currentUser.hasRole("EMPLOYEETYPE_ACCESS")) {
            persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle2").getString("EmployeeTypeDeleted"));
            if (!JsfUtil.isValidationFailed()) {
                selected = null; // Remove selection
                items = null;    // Invalidate list of items to trigger re-query.
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public List<EmployeeType> getItems() {
        if (currentUser.hasRole("EMPLOYEETYPE_ACCESS")) {
            if (items == null) {
                items = getFacade().findAll();
            }
            return items;
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
            return null;
        }
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle2").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle2").getString("PersistenceErrorOccured"));
            }
        }
    }

    public EmployeeType getEmployeeType(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<EmployeeType> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<EmployeeType> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = EmployeeType.class)
    public static class EmployeeTypeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EmployeeTypeController controller = (EmployeeTypeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "employeeTypeController");
            return controller.getEmployeeType(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof EmployeeType) {
                EmployeeType o = (EmployeeType) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), EmployeeType.class.getName()});
                return null;
            }
        }

    }

    public static class Grant implements Serializable {

        private String key;
        private String value;

        public Grant(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }
}
