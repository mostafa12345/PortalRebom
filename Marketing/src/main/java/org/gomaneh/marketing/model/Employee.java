/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.gomaneh.marketing.model.polygon.Polygon;
import org.gomaneh.marketing.model.polygon.Polygon.Builder;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "EMPLOYEE")
@XmlRootElement
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NAME")
    private String name;
    @Size(max = 50)
    @Column(name = "FAMILY")
    private String family;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 128)
    @Column(name = "PHONE")
    private String phone;
    @Size(max = 128)
    @Column(name = "MOBILE")
    private String mobile;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "EMAIL")
    private String email;
    @Size(max = 128)
    @Column(name = "SEX")
    private String sex;
    @Size(max = 512)
    @Column(name = "ADDRESS")
    private String address;
    @Size(max = 1024)
    @Column(name = "PHOTO")
    private String photo;
    @Size(max = 128)
    @Column(name = "NATIONCODE")
    private String nationcode;
    @Column(name = "STATUS")
    private Integer status=0;
    @Size(max = 512)
    @Column(name = "PASSWORD")
    private String password;
    @Size(max = 512)
    @Column(name = "SALT")
    private String salt;
    @OneToMany(mappedBy = "employee")
    private Collection<Orderproduct> orderproductCollection;
    @OneToMany(mappedBy = "parrent")
    private Collection<Employee> employeeCollection;
    @JoinColumn(name = "PARRENT", referencedColumnName = "ID")
    @ManyToOne
    private Employee parrent;
    @Column(name = "PROVINCE")
    private String province;
    @Column(name = "CITY")
    private String city;
    @JoinColumn(name="POSITION",referencedColumnName = "ID")
    @ManyToOne
    private EmployeeType position;
    @Column(name = "META")
    private String description;
    @ElementCollection
    private Map<String,String> roles = new Hashtable<>();
    @ElementCollection
    private Map<String, String> documents;
    @ElementCollection
    private Map<String, String> valueList;
    @Transient
    private String plainPassword;
    @OrderBy("priority")
    @OneToMany(mappedBy = "employee", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Point> points;
    @JoinColumn(name="PROVINCE_ID",referencedColumnName = "ID")
    @ManyToOne
    private Province provinceTmp;
    @OneToMany(mappedBy = "employee")
    private List<Marketplace> marketplaces;
    @Transient
    private Polygon polygon;
    
    @Transient
    public boolean pointInPolygon(double lat,double lng){
        if(polygon==null){
            Builder bulder=Polygon.Builder();
            points.forEach((p) -> {
                bulder.addVertex(new org.gomaneh.marketing.model.polygon.Point(p.getLat(), p.getLng()));
            });
            polygon=bulder.build();
        }
        return polygon.contains(new org.gomaneh.marketing.model.polygon.Point(lat, lng));
    }
    
    public boolean hasRole(String role){
        return roles.containsKey(role);
    }
    public Employee() {
    }

    public List<Marketplace> getMarketplaces() {
        return marketplaces;
    }

    public void setMarketplaces(List<Marketplace> marketplaces) {
        this.marketplaces = marketplaces;
    }
    
    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public Employee(Integer id) {
        this.id = id;
    }

    public Province getProvinceTmp() {
        return provinceTmp;
    }

    public void setProvinceTmp(Province provinceTmp) {
        this.provinceTmp = provinceTmp;
        if(provinceTmp!=null){
            province=provinceTmp.getName();
        }
    }

    public Integer getId() {
        return id;
    }

    public EmployeeType getPosition() {
        return position;
    }

    public void setPosition(EmployeeType position) {
        this.position = position;
    }

    public String jsonPath() {
        StringBuilder out = new StringBuilder();
        int i = 0;
        for (Point p : getPoints()) {
            if (i != 0) {
                out.append(",");
            }
            out.append(String.format("{\"lat\":%s,\"lng\":%s}", p.getLat(), p.getLng()));
            i++;
        }
        return "["+out.toString()+"]";
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNationcode() {
        return nationcode;
    }

    public void setNationcode(String nationcode) {
        this.nationcode = nationcode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getRoles() {
        return roles;
    }

    public void setRoles(Map<String, String> roles) {
        this.roles = roles;
    }

    public Map<String, String> getDocuments() {
        return documents;
    }

    public void setDocuments(Map<String, String> documents) {
        this.documents = documents;
    }

    @XmlTransient
    public Collection<Orderproduct> getOrderproductCollection() {
        return orderproductCollection;
    }

    public void setOrderproductCollection(Collection<Orderproduct> orderproductCollection) {
        this.orderproductCollection = orderproductCollection;
    }

    @XmlTransient
    public Collection<Employee> getEmployeeCollection() {
        return employeeCollection;
    }

    public void setEmployeeCollection(Collection<Employee> employeeCollection) {
        this.employeeCollection = employeeCollection;
    }

    public Employee getParrent() {
        return parrent;
    }

    public void setParrent(Employee parrent) {
        this.parrent = parrent;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name+" "+ family;
    }

    public Map<String, String> getValueList() {
        return valueList;
    }

    public void setValueList(Map<String, String> valueList) {
        this.valueList = valueList;
    }

    public String getPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(String plainPassword) {
        this.plainPassword = plainPassword;
        if (plainPassword != null && !this.plainPassword.isEmpty()) {
            salt = UUID.randomUUID().toString();
            password = org.apache.commons.codec.digest.DigestUtils.sha256Hex(salt + "(" + plainPassword + ")" + salt);
        }
    }

    public boolean checkPassword(String plainPassword){
        String pass=org.apache.commons.codec.digest.DigestUtils.sha256Hex(salt + "(" + plainPassword + ")" + salt);
        return pass.equals(password);
    }
}
