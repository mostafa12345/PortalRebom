function handleSubmit(args, dialog) {
    var jqDialog = jQuery('#' + dialog);
    if (args.validationFailed) {
        jqDialog.effect('shake', {times: 3}, 100);
    } else {
        PF(dialog).hide();
    }
}

function handleSubmitModal(args, dialog) {
    if (!args.validationFailed) {
        $("." + dialog).modal('hide');
    }
}

function handleSubmitAndShow(args, dialog) {
    if (!args.validationFailed) {
        PF(dialog).show();
    }
}

$(document).ready(function () {
    $('[data-toggle="leftSide"]').click(function () {
        if ($('.leftSide').hasClass('close-nav')) {
            $('.leftSide').removeClass('close-nav');
        } else {
            $('.leftSide').addClass('close-nav');
            $('.container').css('padding-right', '0px');
        }
    });
});