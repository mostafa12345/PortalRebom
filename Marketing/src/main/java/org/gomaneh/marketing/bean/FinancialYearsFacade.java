/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.FinancialYears;
import org.gomaneh.marketing.model.Inventory;

/**
 *
 * @author davood
 */
@Stateless
public class FinancialYearsFacade extends AbstractFacade<FinancialYears> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FinancialYearsFacade() {
        super(FinancialYears.class);
    }

    @Override
    public List<FinancialYears> findAll() {
        return em.createQuery("SELECt y FROM FinancialYears y ORDER BY y.id DESC").getResultList();
    }

    public FinancialYears getLastYears() {
        return (FinancialYears) em.createQuery("SELECT y FROM FinancialYears y ORDER BY y.id DESC").setMaxResults(1).getSingleResult();
    }

    public FinancialYears getDefaultYears() {
        try {
            return (FinancialYears) em.createQuery("SELECT y FROM FinancialYears y WHERE y.status=1").setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    public boolean validate(String years) {
        List<FinancialYears> fy=em.createQuery("SELECT y FROM FinancialYears y WHERE y.years >= :years").setParameter("years", years).setMaxResults(1).getResultList();
        return (fy==null||fy.isEmpty());    
    }
    
    public boolean validateEdit(String years){
        List<Inventory> inList=em.createQuery("select i from Inventory i where i.years = :years").setParameter("years", years).setMaxResults(1).getResultList();
        return (inList==null||inList.isEmpty()); 
    }

    public FinancialYears findOpenInventory() {
        try {
            return (FinancialYears) em.createQuery("SELECT y FROM FinancialYears y WHERE y.end=false").setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
}
