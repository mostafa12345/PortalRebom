/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "BASEATTRIBUTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Baseattribute.findAll", query = "SELECT b FROM Baseattribute b")
    , @NamedQuery(name = "Baseattribute.findById", query = "SELECT b FROM Baseattribute b WHERE b.id = :id")
    , @NamedQuery(name = "Baseattribute.findByName", query = "SELECT b FROM Baseattribute b WHERE b.name = :name")
    , @NamedQuery(name = "Baseattribute.findByStatus", query = "SELECT b FROM Baseattribute b WHERE b.status = :status")
    , @NamedQuery(name = "Baseattribute.findByMeta", query = "SELECT b FROM Baseattribute b WHERE b.meta = :meta")
    , @NamedQuery(name = "Baseattribute.findByUpdateat", query = "SELECT b FROM Baseattribute b WHERE b.updateat = :updateat")})
public class Baseattribute implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 128)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private int status = 1;
    @Size(max = 1024)
    @Column(name = "META")
    private String meta;
    @Column(name = "UPDATEAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateat;
    @JoinColumn(name = "EMPLOYEE", referencedColumnName = "ID")
    @ManyToOne
    private Employee employee;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "baseattribute")
    private Collection<BaseattributeMarkrttype> baseattributeMarkrttypeCollection;

    public Baseattribute() {
    }

    public Baseattribute(Integer id) {
        this.id = id;
    }

    public Baseattribute(Integer id, int status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Date getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Date updateat) {
        this.updateat = updateat;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @XmlTransient
    public Collection<BaseattributeMarkrttype> getBaseattributeMarkrttypeCollection() {
        return baseattributeMarkrttypeCollection;
    }

    public void setBaseattributeMarkrttypeCollection(Collection<BaseattributeMarkrttype> baseattributeMarkrttypeCollection) {
        this.baseattributeMarkrttypeCollection = baseattributeMarkrttypeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Baseattribute)) {
            return false;
        }
        Baseattribute other = (Baseattribute) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @PrePersist
    @PreUpdate
    public void preFlush() {
        updateat = new Date();
    }
}
