/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Cartex;
import org.gomaneh.marketing.model.Category;
import org.gomaneh.marketing.model.Product;

/**
 *
 * @author davood
 */
@Stateless
public class CartexFacade extends AbstractFacade<Cartex> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CartexFacade() {
        super(Cartex.class);
    }

    public Cartex findRemain(Product p, String years) {
        try {
            return (Cartex) em.createQuery("SELECT c FROM Cartex c WHERE c.product = :product and c.years = :years").setParameter("product", p).setParameter("years", years).setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Cartex> findByYears(String years) {
        return em.createQuery("SELECT c FROM Cartex c WHERE c.years = :years order by c.remain ASC").setParameter("years", years).getResultList();
    }

    public List<Cartex> findByProduct(Product p) {
        return em.createQuery("SELECT c FROM Cartex c WHERE c.product = :product order by c.remain ASC").setParameter("product", p).getResultList();
    }

    public List<Object[]> findByCategoryInYears(String years) {
        return em.createQuery("SELECT  c.product.category.name ,SUM(c.input),SUM(c.output) FROM Cartex c WHERE  c.years = :years group by c.product.category").setParameter("years", years).getResultList();
    }

    public List<Object[]> groupByYears() {
        return em.createQuery("SELECT c.years,SUM(c.input),SUM(c.output) FROM Cartex c GROUP BY c.years").getResultList();
    }

    public List<Object[]> groupByYears(Product p) {
        return em.createQuery("SELECT c.years,SUM(c.input),SUM(c.output) FROM Cartex c WHERE c.product = :product GROUP BY c.years").setParameter("product", p).getResultList();
    }
    
    public List<Object[]> groupByMonth(String years) {
        return em.createNativeQuery("SELECT MONTH(INVENTORY.REAL_DATE), SUM(CASE WHEN INVENTORY.TYPE=0 THEN INVENTORY.NUMBER ELSE 0 END) as INPUT, SUM(CASE WHEN INVENTORY.TYPE=1 THEN INVENTORY.NUMBER ELSE 0 END) as OUTPUT  FROM `INVENTORY` WHERE INVENTORY.YEARS="+years+" GROUP BY MONTH(INVENTORY.REAL_DATE)").getResultList();
    }
    
    public List<Object[]> groupByMonth(Product p,String years) {
        return em.createNativeQuery("SELECT MONTH(INVENTORY.REAL_DATE), SUM(CASE WHEN INVENTORY.TYPE=0 THEN INVENTORY.NUMBER ELSE 0 END) as INPUT, SUM(CASE WHEN INVENTORY.TYPE=1 THEN INVENTORY.NUMBER ELSE 0 END) as OUTPUT  FROM `INVENTORY` WHERE INVENTORY.YEARS="+years+" AND INVENTORY.PRODUCT="+p.getId()+" GROUP BY MONTH(INVENTORY.REAL_DATE)").getResultList();
    }
    
    public List<Cartex> getZeroProductRemain(String years) {
        return em.createQuery("SELECT c FROM Cartex c WHERE c.remain = 0 and c.years = :years").setParameter("years", years).getResultList();
    }

    public List<Cartex> getLimitProductRemain(String years) {
        return em.createQuery("SELECT c FROM Cartex c WHERE c.remain <= c.product.limit and c.years = :years").setParameter("years", years).getResultList();
    }

}
