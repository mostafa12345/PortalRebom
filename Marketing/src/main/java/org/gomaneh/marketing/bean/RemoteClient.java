/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.websocket.Session;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;

/**
 *
 * @author davood
 */
@PushEndpoint("/api")
public class RemoteClient {

    private static Set<Session> clients
            = Collections.synchronizedSet(new HashSet<Session>());

    @OnMessage(encoders = {JSONEncoder.class})
    public FacesMessage onMessage(FacesMessage message) {
        return message;
    }
}
