/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.controller.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author davood
 */
@FacesValidator(value = "confirmPasswordValidator")
public class ConfirmPasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String val = (String) value;
        String confirm = (String) component.getAttributes().get("confirm");
        if (val == null || confirm == null) {
            return; // Just ignore and let required="true" do its job.
        }

        if (!val.equals(confirm)) {
            throw new ValidatorException(new FacesMessage("رمزهای عبور وارد شده یکسان نیستند."));
        }
    }

}
