package org.gomaneh.marketing.controller;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.gomaneh.marketing.model.Category;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.CategoryFacade;

import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@Named("categoryController")
@ViewScoped
public class CategoryController implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.CategoryFacade ejbFacade;
    private List<Category> items = null;
    private Category selected;
    private TreeNode root;
    private TreeNode selectedNode;
    private Part part;
    private CroppedImage cropper;
    private File temp;

    public CategoryController() {
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
        if (selectedNode != null) {
            selected = (Category) selectedNode.getData();
        }
    }

    public Category getSelected() {
        return selected;
    }

    public void setSelected(Category selected) {
        this.selected = selected;
    }

    public void createTree(TreeNode node, Category ctg) {
        for (Category c : ctg.getCategoryCollection()) {
            TreeNode n = new DefaultTreeNode(c, node);
            n.setExpanded(true);
            createTree(n, c);
        }
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public CroppedImage getCropper() {
        return cropper;
    }

    public void setCropper(CroppedImage cropper) {
        this.cropper = cropper;
    }

    public File getTemp() {
        return temp;
    }

    public void setTemp(File temp) {
        this.temp = temp;
    }

    public TreeNode getRoot() {
        root = new DefaultTreeNode(null, null);
        for (Category c : ejbFacade.parents()) {
            TreeNode n = new DefaultTreeNode(c, root);
            n.setExpanded(true);
            createTree(n, c);
        }
        return root;
    }

    public void onDragDrop(TreeDragDropEvent event) {
        TreeNode dragNode = event.getDragNode();

        TreeNode parent = dragNode.getParent();
        Category src = (Category) dragNode.getData();
        src.setParrent((Category) parent.getData());
        int i = 0;
        for (TreeNode n : parent.getChildren()) {
            if (i == Integer.valueOf(dragNode.getRowKey()) || !dragNode.getData().equals(n.getData())) {
                ((Category) n.getData()).setPriority(i);
                ejbFacade.edit((Category) n.getData());
            }
            i++;
        }
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private CategoryFacade getFacade() {
        return ejbFacade;
    }

    public Category prepareCreate() {
        selected = new Category();
        initializeEmbeddableKey();
        return selected;
    }

    public void uploadImage() throws IOException {
        if (part != null) {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            String home = context.getRealPath("/resources/");
            File dir = new File(home + "/temp");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String name = System.currentTimeMillis() + ".png";
            temp = new File(dir.getAbsolutePath() + "/" + name);
            Files.copy(part.getInputStream(), temp.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
        }
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CategoryCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() throws IOException {
        if (cropper != null) {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(cropper.getBytes()));
            BufferedImage tmp = new BufferedImage(300, 150, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) tmp.getGraphics();
            g.drawImage(img, 0, 0, 300, 150, null);
            g.dispose();
            g.setComposite(AlphaComposite.Src);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            String home = System.getProperty("user.home");
            File dir = new File(home + "/images/thumb");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File out = new File(home + "/images/" + selected.getName() + ".png");
            File outThumb = new File(home + "/images/thumb/" + selected.getName() + ".png");
            if (out.exists()) {
                out.delete();
            }
            if (outThumb.exists()) {
                outThumb.delete();
            }
            ImageIO.write(tmp, "png", outThumb);
            ImageIO.write(img, "png", out);
            if (temp.exists()) {
                temp.delete();
            }
            selected.setImage(selected.getName() + ".png");
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("CategoryUpdated"));
    }

    public void destroy() {
        String home = System.getProperty("user.home");
        File dir = new File(home + "/images/thumb");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File out = new File(home + "/images/" + selected.getName() + ".png");
        File outThumb = new File(home + "/images/thumb/" + selected.getName() + ".png");
        if (out.exists()) {
            out.delete();
        }
        if (outThumb.exists()) {
            outThumb.delete();
        }
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("CategoryDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Category> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Category> getChildList() {
        return getFacade().mainCategories();
    }

    public Category getCategory(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Category> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Category> getItemsAvailableSelectOne() {

        return getFacade().findAll();
    }

    public List<SelectItem> getSelectitemsCategories() {
        List<SelectItem> itemList = new ArrayList<>();
        itemList.add(new SelectItem(null,""));
        getFacade().findAll().forEach((ctg) -> {
            itemList.add(new SelectItem(ctg, ctg.getName()));
        });
        return itemList;
    }

    @FacesConverter(forClass = Category.class)
    public static class CategoryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CategoryController controller = (CategoryController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "categoryController");
            return controller.getCategory(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Category) {
                Category o = (Category) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Category.class.getName()});
                return null;
            }
        }

    }

}
