/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Category;

/**
 *
 * @author davood
 */
@Stateless
public class CategoryFacade extends AbstractFacade<Category> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }
    public List<Category> parents(){
        return em.createQuery("SELECT c FROM Category c where c.parrent IS NULL AND c.status >=0 ORDER BY c.priority").getResultList();
    }
    public List<Category> mainCategories(){
        return em.createQuery("SELECT c FROM Category c where c NOT IN (SELECT g.parrent FROM Category g WHERE g.parrent IS NOT NULL) AND c.status >=0 ORDER BY c.priority").getResultList();
    }
}
