/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "PRODUCTSERIALS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Productserials.findAll", query = "SELECT p FROM Productserials p")
    , @NamedQuery(name = "Productserials.findBySerial", query = "SELECT p FROM Productserials p WHERE p.serial = :serial")
    , @NamedQuery(name = "Productserials.findByStatus", query = "SELECT p FROM Productserials p WHERE p.status = :status")})
public class Productserials implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 512)
    @Column(name = "SERIAL")
    private String serial;
    @Column(name = "STATUS")
    private Integer status=0;
    @JoinColumn(name = "PRODUCT", referencedColumnName = "ID")
    @ManyToOne
    private Product product;

    public Productserials() {
    }

    public Productserials(String serial, Product product) {
        this.serial = serial;
        this.product = product;
    }

    public Productserials(String serial) {
        this.serial = serial;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serial != null ? serial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productserials)) {
            return false;
        }
        Productserials other = (Productserials) object;
        if ((this.serial == null && other.serial != null) || (this.serial != null && !this.serial.equals(other.serial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.Productserials[ serial=" + serial + " ]";
    }
    
}
