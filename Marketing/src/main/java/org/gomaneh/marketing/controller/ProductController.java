package org.gomaneh.marketing.controller;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.ULocale;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.gomaneh.marketing.model.Product;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.ProductFacade;

import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FontCharset;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.gomaneh.marketing.bean.CartexFacade;
import org.gomaneh.marketing.bean.FinancialYearsFacade;
import org.gomaneh.marketing.bean.ProductserialsFacade;
import org.gomaneh.marketing.model.Cartex;
import org.gomaneh.marketing.model.Category;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.model.FinancialYears;
import org.gomaneh.marketing.model.ProductFeature;
import org.gomaneh.marketing.model.Productserials;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@Named("productController")
@ViewScoped
public class ProductController implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.ProductFacade ejbFacade;
    @EJB
    private ProductserialsFacade serialsFacede;
    @EJB
    private FinancialYearsFacade yearsFacade;
    @EJB
    private CartexFacade cartexFacade;
    private List<Product> items = null;
    private Product selected;
    private String page = "product/List";
    private Part file;
    private CroppedImage cropper;
    private File temp;
    private String newFileName;
    private Category categorySelected;
    private Integer numberBarcode;
    private Employee currentUser;
    private String years = "1395";

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
        FinancialYears y = yearsFacade.getDefaultYears();
        if (y == null) {
            try {
                context.getExternalContext().redirect("setting.xhtml");
            } catch (IOException ex) {

            }
        } else {
            years = y.getYears();
        }
    }

    public StreamedContent postProcessXLS() throws FileNotFoundException, IOException {
        String relativeWebPath = "/resources/img/logo.png";
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);
        File file = new File(absoluteDiskPath);

        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet("محصولات");
        XSSFFont font = wb.createFont();
        font.setColor(IndexedColors.BLACK.index);
        font.setFontName("B Nazanin");
        font.setFontHeight(12);
        sheet.getCTWorksheet().getSheetViews().getSheetViewArray(0).setRightToLeft(true);
        XSSFRow title = sheet.createRow(0);
        int cellnum = 8;
        CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, 2, cellnum - 1);
        sheet.addMergedRegion(cellRangeAddress);
        CellRangeAddress cellRangeAddress1 = new CellRangeAddress(1, 5, 2, cellnum - 1);
        int area1 = sheet.addMergedRegion(cellRangeAddress1);
        CellRangeAddress cellRangeAddress2 = new CellRangeAddress(0, 5, 0, 1);
        sheet.addMergedRegion(cellRangeAddress2);
        XSSFCellStyle cellStyle3 = wb.createCellStyle();
        cellStyle3.getFont().setCharSet(FontCharset.ARABIC);
        cellStyle3.setFont(font);
        cellStyle3.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        cellStyle3.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
        title.createCell(0);
        CellRangeAddress merge = sheet.getMergedRegion(area1);
        title.getCell(0).setCellStyle(cellStyle3);
        title.createCell(2).setCellValue("لیست محصولات");
        title.getCell(2).setCellStyle(cellStyle3);
        ULocale ul = new ULocale("fa_IR@calender=persian");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", ul);
        String filter = "تاریخ: " + sdf.format(new Date()) + "\n";
        sheet.createRow(1).createCell(2).setCellValue(filter);
        sheet.getRow(1).getCell(2).setCellStyle(wb.createCellStyle());
        sheet.getRow(1).getCell(2).getCellStyle().setWrapText(true);
        sheet.getRow(1).getCell(2).getCellStyle().setAlignment(XSSFCellStyle.ALIGN_CENTER);
        sheet.getRow(1).getCell(2).getCellStyle().setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        sheet.getRow(1).getCell(2).getCellStyle().setFont(font);
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.CORAL.index);
        cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        cellStyle.setFont(font);
        final FileInputStream stream
                = new FileInputStream(file);
        final Drawing drawing = sheet.createDrawingPatriarch();
        byte[] filebyte = new byte[(int) file.length()];
        XSSFClientAnchor anchor = new XSSFClientAnchor();
        anchor.setCol1(0);
        anchor.setRow1(0);
        stream.read(filebyte);
        final int pictureIndex = wb.addPicture(filebyte, Workbook.PICTURE_TYPE_PNG);
        final Picture pict = drawing.createPicture(anchor, pictureIndex);
        pict.resize(2, 5.3);
        XSSFRow header = sheet.createRow(6);
        String[] hlabel = new String[]{"ردیف", "کد کالا", "نام کالا", "دسته بندی", "قیمت", "ورودی انبار", "خروجی از انبار", "موجودی"};
        for (int i = 0; i < 8; i++) {
            sheet.autoSizeColumn(i);
            XSSFCell cell = header.createCell(i);
            cell.setCellValue(hlabel[i]);
            cell.setCellStyle(cellStyle);
        }
        List<Cartex> vector = cartexFacade.findByYears(years);
        if (vector != null) {
            int j = 7;
            for (Cartex obj : vector) {
                XSSFRow row = sheet.createRow(j);
                XSSFCell cell = row.createCell(0);
                cell.setCellValue(String.valueOf(j - 6));
                XSSFCell cell2 = row.createCell(1);
                cell2.setCellValue(obj.getProduct().getPartnumber());
                XSSFCell cell3 = row.createCell(2);
                cell3.setCellValue(obj.getProduct().getName());
                XSSFCell cell4 = row.createCell(3);
                cell4.setCellValue(obj.getProduct().getCategory().getName());
                XSSFCell cell5 = row.createCell(4);
                cell5.setCellValue(obj.getProduct().getPrice());
                XSSFCell cell6 = row.createCell(5);
                cell6.setCellValue(obj.getInput());
                XSSFCell cell7 = row.createCell(6);
                cell7.setCellValue(obj.getOutput());
                XSSFCell cell8 = row.createCell(7);
                cell8.setCellValue(obj.getRemain());
                for (int i = 0; i < 8; i++) {
                    cell = row.getCell(i);
                    cell.setCellStyle(wb.createCellStyle());
                    cell.getCellStyle().setAlignment(XSSFCellStyle.ALIGN_CENTER);
                    cell.getCellStyle().setVerticalAlignment(XSSFCellStyle.ALIGN_CENTER);
                    cell.getCellStyle().setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                    cell.getCellStyle().setFont(font);
                    if (j % 2 == 0) {
                        cell.getCellStyle().setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
                    } else {
                        cell.getCellStyle().setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
                    }
                }
                j++;
            }
        }
        FileOutputStream outPutStream = null;
        FileInputStream inputStream = null;
        DefaultStreamedContent content=null;
        try {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/media/temp");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            outPutStream = new FileOutputStream(dir.getAbsolutePath() + "/product.xlsx");
            wb.write(outPutStream);
            inputStream = new FileInputStream(dir.getAbsolutePath() + "/product.xlsx");
            content= new DefaultStreamedContent(inputStream, "xlsx", "product.xlsx", "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outPutStream != null) {
                try {
                    outPutStream.flush();
                    outPutStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return content;
    }


    public String exportFunction(Product p) {
        return "11111";
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Category getCategorySelected() {
        return categorySelected;
    }

    public void setCategorySelected(Category categorySelected) {
        this.categorySelected = categorySelected;
    }

    public Integer getNumberBarcode() {
        return numberBarcode;
    }

    public void setNumberBarcode(Integer numberBarcode) {
        this.numberBarcode = numberBarcode;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public void uploadImage() throws IOException {
        if (file != null) {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            String home = context.getRealPath("/resources/");
            File dir = new File(home + "/temp");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String name = System.currentTimeMillis() + ".png";
            temp = new File(dir.getAbsolutePath() + "/" + name);
            Files.copy(file.getInputStream(), temp.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
        }
    }

    public void saveImage() throws IOException {
        if (cropper != null) {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(cropper.getBytes()));
            BufferedImage tmp2 = new BufferedImage(800, 600, BufferedImage.TYPE_INT_RGB);
            BufferedImage tmp = new BufferedImage(300, 225, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = (Graphics2D) tmp2.getGraphics();
            g2.setColor(Color.white);
            g2.fillRect(0, 0, 800, 600);
            g2.drawImage(img, 0, 0, 800, 600, null);
            g2.dispose();
            g2.setComposite(AlphaComposite.Src);
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Graphics2D g = (Graphics2D) tmp.getGraphics();
            g.setColor(Color.white);
            g.fillRect(0, 0, 300, 225);
            g.drawImage(img, 0, 0, 300, 225, null);
            g.dispose();
            g.setComposite(AlphaComposite.Src);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            String home = System.getProperty("user.home");
            File dir = new File(home + "/images/thumb");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String name = selected.getName().replaceAll(" ", "_") + "_" + System.currentTimeMillis() + ".jpg";
            File out = new File(home + "/images/" + name);
            File outThumb = new File(home + "/images/thumb/" + name);
            ImageIO.write(tmp, "jpg", outThumb);
            ImageIO.write(tmp2, "jpg", out);
            if (temp.exists()) {
                temp.delete();
            }
            selected.getImgs().add(name);
            selected.imageListToString();
            update();
        }
    }

    public void removeImage(String image) {
        if (selected != null) {
            String home = System.getProperty("user.home");
            File thumb = new File(home + "/images/thumb/" + image);
            if (thumb.exists()) {
                thumb.delete();
            }
            thumb = new File(home + "/images/" + image);
            if (thumb.exists()) {
                thumb.delete();
            }
            selected.getImgs().remove(image);
            selected.imageListToString();
            update();
        }
    }
    private List<Productserials> serials;

    public List<Productserials> getSerials() {
        if (selected != null) {
            if (serials == null) {
                serials = serialsFacede.findByProduct(selected);
            }
            return serials;
        }
        return null;
    }

    public void generateBarcode() {
        serials = new ArrayList<>();
        ThreadLocalRandom.current().longs(1000000000L, 8999999999L).distinct().limit(numberBarcode).forEach((s) -> {
            serials.add(new Productserials(selected.getPartnumber() + "" + String.valueOf(s), selected));
        });
    }

    public void setSerials(List<Productserials> serials) {
        this.serials = serials;
    }

    public void fileUploadListener(FileUploadEvent event) throws IOException {
        if (event.getFile() != null && selected != null) {
            String name = event.getFile().getFileName();
            String ext = FilenameUtils.getExtension(name);
            name = selected.getName().replaceAll(" ", "_") + "." + ext;
            if (ext.equalsIgnoreCase("mp4")) {
                selected.setVideoMp4(name);
            } else if (ext.equalsIgnoreCase("webm")) {
                selected.setVideoWebm(name);
            } else {
                selected.setCatalog(name);
            }
            String home = System.getProperty("user.home");
            File video = new File(home + "/images/videos/" + name);
            if (!video.exists()) {
                video.mkdirs();
            }
            Files.copy(event.getFile().getInputstream(), video.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
            update();
        }
    }

    public List<String> autoComplateSuggestion(String query) {
        if (selected != null && query != null && !query.isEmpty()) {
            return ejbFacade.findFeatureString(query);
        }
        return null;
    }

    public void reinitFeature() {
        selected.setTempFeature(new ProductFeature(selected));
        update();
    }

    public void removeFeature() {
        update();
    }

    public CroppedImage getCropper() {
        return cropper;
    }

    public void setCropper(CroppedImage cropper) {
        this.cropper = cropper;
    }

    public void setTemp(File temp) {
        this.temp = temp;
    }

    public File getTemp() {
        return temp;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public ProductController() {
    }

    public Product getSelected() {
        return selected;
    }

    public void setSelected(Product selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ProductFacade getFacade() {
        return ejbFacade;
    }

    public Product prepareCreate() {
        selected = new Product();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ProductCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
            selected = null;
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ProductUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ProductDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Product> getItems() {
        return getFacade().findByCategory(categorySelected);
    }

    public List<SelectItem> getInventoryFilter() {
        List<SelectItem> itemList = new ArrayList<>();
        itemList.add(new SelectItem(3, "همه"));
        itemList.add(new SelectItem(-1, "موجودی منفی"));
        itemList.add(new SelectItem(0, "موجودی صفر"));
        itemList.add(new SelectItem(2, "محدوده هشدار"));
        itemList.add(new SelectItem(1, "موجود"));
        return itemList;
    }

    public boolean filterByInventory(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim();

        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }
        int val = Integer.valueOf(filterText);
        switch (val) {
            case -1:
                return ((Product) value).getInventory() < 0;
            case 0:
                return ((Product) value).getInventory() == 0;
            case 1:
                return ((Product) value).getInventory() > 0;
            case 2:
                return ((Product) value).inventoryAlert();
            case 3:
                return true;
        }
        return true;
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        System.out.println(newValue + "      " + oldValue);
        if (newValue != null && !newValue.equals(oldValue)) {
            Product p = getItems().get(event.getRowIndex());
            p.setPrice((Integer) newValue);
            ejbFacade.edit(p);
            JsfUtil.addSuccessMessage("بهای کالا تغییر کرد");
        }
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Product getProduct(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Product> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Product> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Product.class)
    public static class ProductControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProductController controller = (ProductController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "productController");
            return controller.getProduct(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Product) {
                Product o = (Product) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Product.class.getName()});
                return null;
            }
        }

    }
}
