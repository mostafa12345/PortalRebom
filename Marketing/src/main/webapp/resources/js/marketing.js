/* global context_path, editLat, editLng, address, province, city, selectedLat, selectedLng, has_role_self_add, path, OmniFaces, google, has_role_all_add */
var map;
var autocomplete;
var geocoder;
var mypos = {};
var region;
var markerMe;
var markersList = [];
var markers = [];
var full = false;
function handleMessage() {
    var data = OmniFaces.Ajax.data;
    markers = $.parseJSON(data.markers);
    $.each(markersList, function (m) {
        markersList[m].setMap(null);
    });
    $.each(markers, function (m) {
        markersList.push(createMarker(markers[m], map));
    });
}

function marketSelect() {
    var data = OmniFaces.Ajax.data;
    var market = $.parseJSON(data.market);
    $('#market_title').text(market.title);
    $('#market_type').text(market.type);
    $('.notificationBar').slideDown('fast');
}
function initMap() {
    var latLng = {lat: -25.363, lng: 131.044};
    var options = {
        zoom: 15,
        center: latLng
    };
    //region = new Region(path);
    map = new google.maps.Map(document.getElementById('map'), options);
    if (path && path.length > 3) {
        map.setCenter(center(path));
        callMarkers();
    } 
    if (navigator.geolocation) {
        markerMe = new google.maps.Marker();
        markerMe.setIcon(context_path + '/resources/img/current.png');
        navigator.geolocation.getCurrentPosition(function (position) {
            showPosition(position);
        }, function (e) {
            $.getJSON("http://ipinfo.io", function (ipinfo) {
                var latLng = ipinfo.loc.split(",");
                showPosition({coords: {latitude: Number(latLng[0]), longitude: Number(latLng[1])}});
            });
        });
        navigator.geolocation.watchPosition(showPosition);
    } else {
        $.getJSON("http://ipinfo.io", function (ipinfo) {
            var latLng = ipinfo.loc.split(",");
            showPosition({coords: {latitude: Number(latLng[0]), longitude: Number(latLng[1])}});
        });
    }
    if (path && path.length > 3) {
        var range = createShape(path, map);
        if (has_role_self_add) {
            range.addListener('dblclick', function (e) {
                handlePointClick(e);
            });
        }
    }
    var input = document.getElementById('searchTextField');
    var options = {
        componentRestrictions: {country: 'ir'}
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        serachLocation();
    });
    geocoder = new google.maps.Geocoder();
    if (has_role_all_add) {
        map.addListener('dblclick', function (e) {
            handlePointClick(e);
        });
    }
}

var getDistance = function (p1, p2) {
    function rad(x) {
        return x * Math.PI / 180;
    }
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat - p1.lat);
    var dLong = rad(p2.lng - p1.lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
};

function showPosition(position) {
    var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
    };
    if (full) {
        map.setCenter(pos);
        markerMe.setPosition(pos);
        markerMe.setMap(map);
        if (getDistance(mypos, pos) > 300) {
            selectedLat.value = pos.lat;
            selectedLng.value = pos.lng;
            mypos = pos;
            callMarkers();
        }
    } else {
        mypos = pos;
        map.setCenter(pos);
    }
}

function serachLocation() {
    var place = autocomplete.getPlace();
    map.setCenter(place.geometry.location);
}

var center = function (arr) {
    var minX, maxX, minY, maxY;
    for (var i = 0; i < arr.length; i++) {
        minX = (arr[i].lat < minX || minX == null) ? arr[i].lat : minX;
        maxX = (arr[i].lat > maxX || maxX == null) ? arr[i].lat : maxX;
        minY = (arr[i].lng < minY || minY == null) ? arr[i].lng : minY;
        maxY = (arr[i].lng > maxY || maxY == null) ? arr[i].lng : maxY;
    }
    return {"lat": (minX + maxX) / 2, "lng": (minY + maxY) / 2};
};

var inRegion = function (x, y, points) {
    var inside = false;
    for (var i = 0, j = points.length - 1; i < points.length; j = i++) {
        var xi = points[i].lat, yi = points[i].lng;
        var xj = points[j].lat, yj = points[j].lng;
        var intersect = ((yi > y) !== (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect)
            inside = !inside;
    }

    return inside;
};

var currentMarker = null;
var latlng;
function handlePointClick(event) {
    editLat.value = event.latLng.lat();
    editLng.value = event.latLng.lng();
    latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
    if (confirm("آیا این مکان برای ثبت مورد تایید است؟")) {
        prepareCreate();
    }
}

function getAddress(latLng) {
    geocoder.geocode({'latLng': latLng},
            function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        address.value = results[0].formatted_address;
                        var l = results[0].address_components.length;
                        province.value = results[0].address_components[l - 2].long_name;
                        city.value = results[0].address_components[l - 3].long_name;
                        console.log(l);
                    }
                } else {

                }
            }
    );
}
function markerAddComplete() {
    getAddress(latlng);
    $('.mymodal').show();
}

function cancel() {
    return false;
}
function cancelForm() {
    $('.mymodal').hide();
}

function finishSubmit(data) {
    cancelForm();
    var market = $.parseJSON(data);
    markersList.push(createMarker(market, map));
    markers.push(market);
}
$(document).ready(function () {
    $('#search_btn').click(function () {
        if ($('.searchbar').hasClass('open-search')) {
            $('.searchbar').removeClass('open-search');
        } else {
            $('.searchbar').addClass('open-search');
        }
    });
});
var prev_infowindow = false;
function createMarker(market, map) {
    var marker = new google.maps.Marker(market);
    marker.setMap(map);
    marker.setIcon(context_path + "/media/" + market.icon);
    var contentString = "<h2>" + market.title + "</h2><p>" + market.market_type + "</p>";
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(marker, "click", function (e) {
        if (prev_infowindow) {
            prev_infowindow.close();
        }
        prev_infowindow = infowindow;
        infowindow.open(map, marker);
    });
    marker.addListener('dblclick', function (e) {
        selectedLat.value = e.latLng.lat();
        selectedLng.value = e.latLng.lng();
        selectMarker();
        $('.notificationBar').slideUp('fast');
    });
    return marker;
}
function createShape(d, map) {
    var s = new google.maps.Polygon({
        paths: d,
        strokeColor: '#00ff00',
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: '#00ff00',
        fillOpacity: 0.1,
        zIndex: -1
    });
    s.setMap(map);
    return s;
}