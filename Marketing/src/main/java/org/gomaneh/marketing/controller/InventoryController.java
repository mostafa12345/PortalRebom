package org.gomaneh.marketing.controller;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.ULocale;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.gomaneh.marketing.model.Inventory;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.InventoryFacade;

import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.validator.ValidatorException;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FontCharset;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.gomaneh.marketing.bean.CartexFacade;
import org.gomaneh.marketing.bean.FinancialYearsFacade;
import org.gomaneh.marketing.model.Cartex;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.model.FinancialYears;
import org.gomaneh.marketing.model.Product;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Servlets;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

@Named("inventoryController")
@ViewScoped
public class InventoryController extends LazyDataModel<Inventory>
        implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.InventoryFacade ejbFacade;
    @EJB
    private FinancialYearsFacade yearsFacade;
    @EJB
    private org.gomaneh.marketing.bean.ProductFacade productFacade;
    @EJB
    private CartexFacade cartexFacade;
    private List<Inventory> items = null;
    private Inventory selected;
    private Inventory temp;
    private int type = 0;
    private Product product;
    private String page = "List";
    private Part filePart;
    private Employee currentUser;

    private String years = "1395";
    private FinancialYears financialYears;

    //filter parameter
    private Product productFilter;
    private Integer numberFilter;
    private Integer numberStateFilter;
    private Date startFilter;
    private Date endFilter;
    private boolean dateStateFilter = false;
    private Employee employeeFilter;
    private Employee inventoryFilter;
    private List<Cartex> cartexList;

    //chart parameter
    private BarChartModel barModel;
    private PieChartModel pieModel1;
    private Product productSelected;
    private String yearsSelected;
    private String[] month;

    @PostConstruct
    public void init() {
        selected = new Inventory();
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
        String param = context.getExternalContext().getRequestParameterMap().get("type");
        if (param != null) {
            if (param.equalsIgnoreCase("input")) {
                type = 1;
            } else if (param.equalsIgnoreCase("output")) {
                type = 0;
            }
        }
        financialYears = yearsFacade.getDefaultYears();
        if (financialYears == null) {
            try {
                context.getExternalContext().redirect("setting.xhtml");
            } catch (IOException ex) {

            }
        } else {
            years = financialYears.getYears();
        }
        month=new String[]{"ژانویه","فوریه","مارس","آوریل","مه","ژوئن","ژوئیه","اوت","سپتامبر","اکتبر","نوامبر","دسامبر"};
    }

    @Override
    public List<Inventory> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        setRowCount(ejbFacade.count(first, pageSize, type, years, productFilter, numberFilter, numberStateFilter, startFilter, endFilter, dateStateFilter, employeeFilter, inventoryFilter));
        return ejbFacade.load(first, pageSize, multiSortMeta, type, years, productFilter, numberFilter, numberStateFilter, startFilter, endFilter, dateStateFilter, employeeFilter, inventoryFilter); //To change body of generated methods, choose Tools | Templates.
    }

    public void postProcessXLS(Object document) throws FileNotFoundException, IOException {
        String relativeWebPath = "/resources/img/logo.png";
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);
        File file = new File(absoluteDiskPath);
        XSSFWorkbook wb = (XSSFWorkbook) document;
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont font = wb.createFont();
        font.setColor(IndexedColors.BLACK.index);
        font.setFontName("B Nazanin");
        font.setFontHeight(12);
        sheet.getCTWorksheet().getSheetViews().getSheetViewArray(0).setRightToLeft(true);
        XSSFRow header = sheet.getRow(1);
        XSSFRow title = sheet.createRow(0);
        sheet.shiftRows(1, sheet.getPhysicalNumberOfRows() - 1, 5);
        int cellnum = header.getPhysicalNumberOfCells();
        CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, 2, cellnum - 1);
        sheet.addMergedRegion(cellRangeAddress);
        CellRangeAddress cellRangeAddress1 = new CellRangeAddress(1, 5, 2, cellnum - 1);
        int area1 = sheet.addMergedRegion(cellRangeAddress1);
        CellRangeAddress cellRangeAddress2 = new CellRangeAddress(0, 5, 0, 1);
        sheet.addMergedRegion(cellRangeAddress2);
        XSSFCellStyle cellStyle3 = wb.createCellStyle();
        cellStyle3.getFont().setCharSet(FontCharset.ARABIC);
        cellStyle3.setFont(font);
        cellStyle3.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        cellStyle3.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
        title.createCell(0);
        CellRangeAddress merge = sheet.getMergedRegion(area1);
        title.getCell(0).setCellStyle(cellStyle3);
        title.createCell(2).setCellValue("گزارش ");
        title.getCell(2).setCellStyle(cellStyle3);
        ULocale ul = new ULocale("fa_IR@calender=persian");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", ul);
        String filter = "تاریخ: " + sdf.format(new Date()) + "\n";
        sheet.createRow(1).createCell(2).setCellValue(filter);
        sheet.getRow(1).getCell(2).setCellStyle(wb.createCellStyle());
        sheet.getRow(1).getCell(2).getCellStyle().setWrapText(true);
        sheet.getRow(1).getCell(2).getCellStyle().setAlignment(XSSFCellStyle.ALIGN_CENTER);
        sheet.getRow(1).getCell(2).getCellStyle().setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        sheet.getRow(1).getCell(2).getCellStyle().setFont(font);
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.CORAL.index);
        cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        cellStyle.setFont(font);
        final FileInputStream stream
                = new FileInputStream(file);
        final Drawing drawing = sheet.createDrawingPatriarch();
        byte[] filebyte = new byte[(int) file.length()];
        XSSFClientAnchor anchor = new XSSFClientAnchor();
        anchor.setCol1(0);
        anchor.setRow1(0);
        stream.read(filebyte);
        final int pictureIndex = wb.addPicture(filebyte, Workbook.PICTURE_TYPE_PNG);
        final Picture pict = drawing.createPicture(anchor, pictureIndex);
        pict.resize(2, 5.3);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            sheet.autoSizeColumn(i);
            XSSFCell cell = header.getCell(i);
            cell.setCellStyle(cellStyle);
        }

        for (int j = 7; j < sheet.getPhysicalNumberOfRows() + 4; j++) {
            XSSFRow row = sheet.getRow(j);
            for (int i = 0; i < cellnum; i++) {
                XSSFCell cell = row.getCell(i);
                cell.setCellStyle(wb.createCellStyle());
                cell.getCellStyle().setAlignment(XSSFCellStyle.ALIGN_CENTER);
                cell.getCellStyle().setVerticalAlignment(XSSFCellStyle.ALIGN_CENTER);
                cell.getCellStyle().setFont(font);
                cell.getCellStyle().setWrapText(true);
                if (j % 2 == 0) {
                    cell.getCellStyle().setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
                    cell.getCellStyle().setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                } else {
                    cell.getCellStyle().setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
                    cell.getCellStyle().setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                }
            }
        }
    }

    public String getPage() {
        return page;
    }

    public Part getFilePart() {
        return filePart;
    }

    public List<Cartex> getCartexList() {
        if (cartexList == null) {
            cartexList = cartexFacade.findByYears(years);
        }
        return cartexList;
    }

    public List<Cartex> getZeroProductRemain() {
        return cartexFacade.getZeroProductRemain(years);
    }

    public List<Cartex> getLimitProductRemain() {
        return cartexFacade.getLimitProductRemain(years);
    }

    public BarChartModel getBarModel() {
        barModel = initBarModel();
        return barModel;
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
        ChartSeries d1 = new ChartSeries();
        ChartSeries d2 = new ChartSeries();
        if (yearsSelected == null && productSelected == null) {
            model.setTitle("نمودار تراکنش های انبار در سال های مختلف");
            List<Object[]> data = cartexFacade.groupByYears();
            if (data != null) {
                d1.setLabel("ورودی انبار");
                d2.setLabel("خروجی انبار");
                for (Object[] obj : data) {
                    d1.set(obj[0], (Number) obj[1]);
                    d2.set(obj[0], (Number) obj[2]);
                }
            }
        } else if (yearsSelected == null && productSelected != null) {
            model.setTitle("نمودار تراکنش های انبار در سال های مختلف برای کالای " + productSelected.getName());
            List<Object[]> data = cartexFacade.groupByYears(productSelected);
            if (data != null) {
                d1.setLabel("ورودی انبار");
                d2.setLabel("خروجی انبار");
                for (Object[] obj : data) {
                    d1.set(obj[0], (Number) obj[1]);
                    d2.set(obj[0], (Number) obj[2]);
                }
            }
        } else if (yearsSelected != null && productSelected == null) {
            model.setTitle("نمودار تراکنش های انبار در سال های " + yearsSelected);
            List<Object[]> data = cartexFacade.groupByMonth(yearsSelected);
            if (data != null) {
                d1.setLabel("ورودی انبار");
                d2.setLabel("خروجی انبار");
                for (Object[] obj : data) {
                    d1.set(month[(Integer)obj[0]], (Number) obj[1]);
                    d2.set(month[(Integer)obj[0]], (Number) obj[2]);
                }
            }
        } else {
            model.setTitle("نمودار تراکنش های انبار در سال های " + yearsSelected + " برای محصول" + productSelected.getName());
            List<Object[]> data = cartexFacade.groupByMonth(productSelected, yearsSelected);
            if (data != null) {
                d1.setLabel("ورودی انبار");
                d2.setLabel("خروجی انبار");
                for (Object[] obj : data) {
                    d1.set(month[(Integer)obj[0]], (Number) obj[1]);
                    d2.set(month[(Integer)obj[0]], (Number) obj[2]);
                }
            }
        }
        if (d1.getData().isEmpty()) {
            d1.set("", 0);
        }
        if (d2.getData().isEmpty()) {
            d2.set("", 0);
        }
        model.setLegendPosition("ne");
        model.addSeries(d1);
        model.addSeries(d2);
        return model;
    }

    public PieChartModel getPieModel1() {
        pieModel1 = new PieChartModel();
        List<Object[]> data = cartexFacade.findByCategoryInYears(years);
        if (data != null) {
            for (Object[] obj : data) {
                pieModel1.set((String) obj[0], (Number) obj[2]);
                System.out.println("fddsddsd"+obj[2]);
            }
        }

        pieModel1.setTitle("فروش هر دسته بندی");
        pieModel1.setLegendPosition("w");
        return pieModel1;
    }

    public Product getProductSelected() {
        return productSelected;
    }

    public void setProductSelected(Product productSelected) {
        this.productSelected = productSelected;
    }

    public String getYearsSelected() {
        return yearsSelected;
    }

    public void setYearsSelected(String yearsSelected) {
        this.yearsSelected = yearsSelected;
    }

    public void setFilePart(Part filePart) {
        this.filePart = filePart;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public FinancialYears getFinancialYears() {
        return financialYears;
    }

    public void setFinancialYears(FinancialYears financialYears) {
        this.financialYears = financialYears;
    }

    public InventoryController() {
    }

    public Inventory getSelected() {
        return selected;
    }

    public int getType() {
        return type;
    }

    public Product getProduct() {
        return product;
    }

    public Inventory getTemp() {
        return temp;
    }

    public void setTemp(Inventory temp) {
        this.temp = temp;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setSelected(Inventory selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private InventoryFacade getFacade() {
        return ejbFacade;
    }

    public Inventory prepareCreate() {
        selected = new Inventory();
        initializeEmbeddableKey();
        return selected;
    }

    public boolean filterProduct(Object value, Object filter, Locale locale) {
        System.out.println(value);
        return true;
    }

    public void validateNumber(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
        Integer model = (Integer) value;

        if (model <= 0) {
            FacesMessage msg = new FacesMessage(
                    "تعداد کالای انتخابی نمی تواند کوچک تر از یک باشد.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
        if (type == 1) {
            UIInput sd = (UIInput) comp.getAttributes().get("product");
            Product productSelected = (Product) sd.getValue();
            Cartex numberList = cartexFacade.findRemain(productSelected, years);
            if (numberList != null) {
                if (model > numberList.getRemain()) {
                    FacesMessage msg = new FacesMessage(
                            "حواله درخواستی از موجودی انبار بیشتر است.");
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(msg);
                }
            } else {
                FacesMessage msg = new FacesMessage(
                        "این کالا در انبار تعریف نشده است");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

    public boolean hasAttachment() {
        if (temp != null && temp.getDoc() != null && !temp.getDoc().isEmpty()) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/media/inventory/");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File doc = new File(dir.getAbsolutePath() + "/" + temp.getDoc());
            if (doc.exists()) {
                return true;
            }
        }
        return false;
    }

    public void create() throws IOException {
        selected.setEmployee(currentUser);
        selected.setType((short) type);
        selected.setYears(years);
        selected.setReceiverName(selected.getReceiver().getName() + " " + selected.getReceiver().getFamily() + "(" + selected.getReceiver().getMobile() + ")");
        if (filePart != null) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/media/inventory/");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String name = Servlets.getSubmittedFileName(filePart);
            String ext = FilenameUtils.getExtension(name);
            name = System.currentTimeMillis() + "." + ext;
            selected.setDoc(name);
            Files.copy(filePart.getInputStream(), (new File(dir.getAbsolutePath() + "/" + name)).toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle3").getString("InventoryCreated"));

        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
            filePart = null;
            cartexList = null;
            selected = new Inventory();
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle3").getString("InventoryUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle3").getString("InventoryDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Inventory> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction == PersistAction.UPDATE) {
                    getFacade().edit(selected);
                } else if (persistAction == PersistAction.CREATE) {
                    getFacade().create(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle3").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle3").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Inventory getInventory(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Inventory> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Inventory> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    public Employee getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Employee currentUser) {
        this.currentUser = currentUser;
    }

    public Product getProductFilter() {
        return productFilter;
    }

    public void setProductFilter(Product productFilter) {
        this.productFilter = productFilter;
    }

    public Integer getNumberFilter() {
        return numberFilter;
    }

    public void setNumberFilter(Integer numberFilter) {
        this.numberFilter = numberFilter;
    }

    public Integer getNumberStateFilter() {
        return numberStateFilter;
    }

    public void setNumberStateFilter(Integer numberStateFilter) {
        this.numberStateFilter = numberStateFilter;
    }

    public Date getStartFilter() {
        return startFilter;
    }

    public void setStartFilter(Date startFilter) {
        this.startFilter = startFilter;
    }

    public Date getEndFilter() {
        return endFilter;
    }

    public void setEndFilter(Date endFilter) {
        this.endFilter = endFilter;
    }

    public boolean getDateStateFilter() {
        return dateStateFilter;
    }

    public void setDateStateFilter(boolean dateStateFilter) {
        this.dateStateFilter = dateStateFilter;
    }

    public Employee getEmployeeFilter() {
        return employeeFilter;
    }

    public void setEmployeeFilter(Employee employeeFilter) {
        this.employeeFilter = employeeFilter;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Employee getInventoryFilter() {
        return inventoryFilter;
    }

    public void setInventoryFilter(Employee inventoryFilter) {
        this.inventoryFilter = inventoryFilter;
    }

    @FacesConverter(forClass = Inventory.class)
    public static class InventoryControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            InventoryController controller = (InventoryController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "inventoryController");
            return controller.getInventory(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Inventory) {
                Inventory o = (Inventory) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Inventory.class.getName()});
                return null;
            }
        }

    }

}
