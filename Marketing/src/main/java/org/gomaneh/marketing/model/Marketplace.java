/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "MARKETPLACE")
@XmlRootElement
public class Marketplace implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NAME")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LATITUDE")
    private Double latitude;
    @Column(name = "LONGITUDE")
    private Double longitude;
    @JoinColumn(name = "EMPLOYEE", referencedColumnName = "ID")
    @ManyToOne
    private Employee employee;
    @Column(name = "UPDATEAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateat;
    @Column(name = "STATUS")
    private Integer status = 1;
    @Column(name = "PRIORTY")
    private Integer priorty;
    @Size(max = 256)
    @Column(name = "PROVINCE")
    private String province;
    @Size(max = 256)
    @Column(name = "CITY")
    private String city;
    @Size(max = 1024)
    @Column(name = "ADDRESS")
    private String address;
    @JoinColumn(name = "MARKRTTYPE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Markettype markrttype;
    @OrderBy("priorty")
    @OneToMany(mappedBy = "marketplace", cascade = CascadeType.ALL)
    private List<Feature> features = new ArrayList<>();
    @Transient
    private Collection<BaseattributeMarkrttype> baseAttr = new ArrayList<>();
    @Transient
    private Collection<BaseattributeMarkrttype> placesAttr = new ArrayList<>();
    @Transient
    private Collection<BaseattributeMarkrttype> competitionAttr = new ArrayList<>();
    @Transient
    private Collection<Feature> baseFeature = new ArrayList<>();
    @Transient
    private Collection<Feature> placesFeature = new ArrayList<>();
    @Transient
    private Collection<Feature> competitionFeature = new ArrayList<>();

    public Marketplace() {
    }

    public Marketplace(Integer id) {
        this.id = id;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Date updateat) {
        this.updateat = updateat;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPriorty() {
        return priorty;
    }

    public void setPriorty(Integer priorty) {
        this.priorty = priorty;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Markettype getMarkrttype() {
        return markrttype;
    }

    public void setMarkrttype(Markettype markrttype) {
        this.markrttype = markrttype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Marketplace)) {
            return false;
        }
        Marketplace other = (Marketplace) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.Marketplace[ id=" + id + " ]";
    }

    @PrePersist
    @PreUpdate
    public void preFlush() {
        updateat = new Date();
    }

    public Collection<BaseattributeMarkrttype> getBaseAttr() {
        if (baseAttr.isEmpty()) {
            baseAttr = new ArrayList<>();
            if (id == null) {
                baseAttr.addAll(markrttype.getBaseAttr());
            } else {
                for (Feature b : features) {
                    if (b.getType().getGroup() == 1) {
                        baseAttr.add(b.getType());
                    }
                }
            }
        }
        return baseAttr;
    }

    public Collection<BaseattributeMarkrttype> getPlacesAttr() {
        if (placesAttr.isEmpty()) {
            placesAttr = new ArrayList<>();
            if (id == null) {
                placesAttr.addAll(markrttype.getPlacesAttr());
            } else {
                for (Feature b : features) {
                    if (b.getType().getGroup() == 2) {
                        placesAttr.add(b.getType());
                    }
                }
            }
        }
        return placesAttr;
    }

    public Collection<BaseattributeMarkrttype> getCompetitionAttr() {
        if (competitionAttr.isEmpty()) {
            competitionAttr = new ArrayList<>();
            if (id == null) {
                competitionAttr.addAll(markrttype.getCompetitionAttr());
            } else {
                for (Feature b : features) {
                    if (b.getType().getGroup() == 3) {
                        competitionAttr.add(b.getType());
                    }
                }
            }
        }
        return competitionAttr;
    }

    public Collection<Feature> getBaseFeature() {
        if (baseFeature.isEmpty()) {
            baseFeature = new ArrayList<>();
            for (Feature b : features) {
                if (b.getType().getGroup() == 1) {
                    baseFeature.add(b);
                }
            }
        }
        return baseFeature;
    }

    public Collection<Feature> getPlacesFeature() {
        if (placesFeature.isEmpty()) {
            placesFeature = new ArrayList<>();
            for (Feature b : features) {
                if (b.getType().getGroup() == 2) {
                    placesFeature.add(b);
                }
            }
        }
        return placesFeature;
    }

    public Collection<Feature> getCompetitionFeature() {
        if (competitionFeature.isEmpty()) {
            competitionFeature = new ArrayList<>();
            for (Feature b : features) {
                if (b.getType().getGroup() == 3) {
                    competitionFeature.add(b);
                }
            }
        }
        return competitionFeature;
    }

}
