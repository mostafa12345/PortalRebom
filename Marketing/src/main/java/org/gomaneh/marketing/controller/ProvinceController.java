package org.gomaneh.marketing.controller;

import org.gomaneh.marketing.model.Province;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.ProvinceFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.view.ViewScoped;
import org.gomaneh.marketing.bean.CityFacade;
import org.gomaneh.marketing.model.City;

@Named("provinceController")
@ViewScoped
public class ProvinceController implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.ProvinceFacade ejbFacade;
    @EJB
    private CityFacade cityFacate;
    private List<Province> items = null;
    private Province selected;
    private City citySelected;

    public ProvinceController() {
    }

    public Province getSelected() {
        return selected;
    }

    public void setSelected(Province selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ProvinceFacade getFacade() {
        return ejbFacade;
    }

    public Province prepareCreate() {
        selected = new Province();
        initializeEmbeddableKey();
        return selected;
    }

    public void prepareCreateCity(){
        citySelected=new City();
        citySelected.setProvince(selected);
    }
    
    public void createCity(){
        if(selected.getCityCollection()==null){
            selected.setCityCollection(new ArrayList<City>());
        }
        selected.getCityCollection().add(citySelected);
        update();
    }
    
    public void destroyCIty(){
        selected.getCityCollection().remove(citySelected);
        cityFacate.remove(citySelected);
        update();
    }
    
    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ProvinceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ProvinceUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ProvinceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Province> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    public City getCitySelected() {
        return citySelected;
    }

    public void setCitySelected(City citySelected) {
        this.citySelected = citySelected;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Province getProvince(java.lang.Integer id) {
        return getFacade().find(id);
    }
    
    public City getCity(java.lang.Integer id) {
        return cityFacate.find(id);
    }
    
    public List<Province> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Province> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Province.class)
    public static class ProvinceControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProvinceController controller = (ProvinceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "provinceController");
            return controller.getProvince(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Province) {
                Province o = (Province) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Province.class.getName()});
                return null;
            }
        }

    }
    @FacesConverter(forClass = City.class)
    public static class CityControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProvinceController controller = (ProvinceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "provinceController");
            return controller.getCity(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof City) {
                City o = (City) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), City.class.getName()});
                return null;
            }
        }

    }
}
