/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Feature;

/**
 *
 * @author davood
 */
@Stateless
public class FeatureFacade extends AbstractFacade<Feature> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FeatureFacade() {
        super(Feature.class);
    }
    
}
