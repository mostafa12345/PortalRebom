package org.gomaneh.marketing.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.EmployeeFacade;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import org.apache.commons.io.FilenameUtils;
import org.gomaneh.marketing.bean.ProvinceFacade;
import org.gomaneh.marketing.model.Point;
import org.gomaneh.marketing.model.Province;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Servlets;
import org.primefaces.context.RequestContext;

@Named("employeeController")
@ViewScoped
public class EmployeeController implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.EmployeeFacade ejbFacade;
    private List<Employee> items = null;
    private Employee selected;
    private String page = "employee/List";
    private Province province;
    @EJB
    private ProvinceFacade provinceFacade;
    private Part image;
    private UIData component;
    private List<Part> files;
    private String docValue;
    private String docKey;
    private List<String> marktingRoles;

    private Employee currentUser;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
    }

    public EmployeeController() {
    }

    public void imageUpload() throws IOException {
        if (currentUser.hasRole("EMPLOYEE_EDIT")) {
            if (image != null) {
                String home = System.getProperty("user.home");
                File dir = new File(home + "/media/employee/image/");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                BufferedImage img = ImageIO.read(this.image.getInputStream());
                int w = img.getWidth();
                int h = img.getHeight();

                String name = Servlets.getSubmittedFileName(image);
                String ext = FilenameUtils.getExtension(name);
                BufferedImage tmp = new BufferedImage(450, 600, BufferedImage.TYPE_INT_RGB);
                int x = 0, y = 0, w2, h2;
                float r = ((float) w / (float) h);
                if (r >= 0.75) {
                    float rx = (450F / (float) w);
                    y = Math.round((600 - (rx * (float) h)) / 2);
                    w2 = 450;
                    h2 = (int) (h * rx);
                } else {
                    float ry = (600F / (float) h);
                    x = Math.round((450 - (ry * (float) w)) / 2);
                    w2 = (int) (w * ry);
                    h2 = 600;
                }
                Graphics2D g = (Graphics2D) tmp.getGraphics();
                g.setColor(new Color(255, 255, 255));
                g.fillRect(0, 0, 450, 600);
                g.drawImage(img, x, y, w2, h2, null);
                g.dispose();
                name = selected.getId() + "_" + name.replace(ext, "jpg");
                ImageIO.write(tmp, "JPG", new FileOutputStream(dir.getAbsolutePath() + "/" + name));
                selected.setPhoto(name);
                Ajax.update(component.getClientId() + ":" + items.indexOf(selected) + ":imageLink");
                persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public Employee getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Employee currentUser) {
        this.currentUser = currentUser;
    }

    public void upload() throws IOException, InterruptedException {
        if (currentUser.hasRole("EMPLOYEE_EDIT")) {
            if (files != null) {
                if (selected.getDocuments() == null) {
                    selected.setDocuments(new HashMap<>());
                }
                String home = System.getProperty("user.home");
                File dir = new File(home + "/media/employee/document/" + selected.getId());
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                for (Part file : files) {
                    String name = Servlets.getSubmittedFileName(file);
                    String ext = FilenameUtils.getExtension(name);
                    Files.copy(file.getInputStream(), (new File(dir.getAbsolutePath() + "/" + name)).toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                    if (!selected.getDocuments().containsKey(name)) {
                        selected.getDocuments().put(name, name.replace(ext, ""));
                    }
                }
                persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public String getDocValue() {
        if (currentUser.hasRole("EMPLOYEE_SHOW")) {
            if (selected != null && selected.getDocuments() != null && !selected.getDocuments().isEmpty() && docKey != null && !docKey.isEmpty()) {
                docValue = selected.getDocuments().get(docKey);
            }
            return docValue;
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
            return null;
        }
    }

    public void replaceDoc(String key) throws IOException {
        if (currentUser.hasRole("EMPLOYEE_EDIT")) {
            if (image != null) {
                if (selected.getDocuments() == null) {
                    selected.setDocuments(new HashMap<>());
                }
                String home = System.getProperty("user.home");
                File dir = new File(home + "/media/employee/document/" + selected.getId());
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                String name = Servlets.getSubmittedFileName(image);
                String ext = FilenameUtils.getExtension(name);
                Files.copy(image.getInputStream(), (new File(dir.getAbsolutePath() + "/" + name)).toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                String val = selected.getDocuments().get(key);
                selected.getDocuments().remove(key);
                selected.getDocuments().put(name, val);
                persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void removeDocument(String key) {
        if (currentUser.hasRole("EMPLOYEE_EDIT")) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/media/employee/document/" + selected.getId() + "/" + key);
            if (dir.exists()) {
                dir.delete();
            }
            selected.getDocuments().remove(key);
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public String extension(String filename) {
        String ext = FilenameUtils.getExtension(filename);
        if (ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("gif")) {
            return "<span style=\"font-size: 60px\" class='fa fa-3x fa-file-picture-o' ></span>";
        } else if (ext.equalsIgnoreCase("pdf")) {
            return "<span style=\"font-size: 60px\" class='fa fa-3x fa-file-pdf-o' ></span>";
        } else if (ext.equalsIgnoreCase("doc") && ext.equalsIgnoreCase("docx")) {
            return "<span style=\"font-size: 60px\" class='fa fa-3x fa-file-word-o' ></span>";
        } else if (ext.equalsIgnoreCase("xls") && ext.equalsIgnoreCase("xlsx")) {
            return "<span style=\"font-size: 60px\" class='fa fa-3x fa-file-excel-o' ></span>";
        } else if (ext.equalsIgnoreCase("ppt") && ext.equalsIgnoreCase("pptx")) {
            return "<span style=\"font-size: 60px\" class='fa fa-3x fa-file-powerpoint-o' ></span>";
        } else {
            return "<span style=\"font-size: 60px\" class='fa fa-3x fa-file-file' ></span>";
        }
    }

    public void saveShape() {
        if (currentUser.hasRole("EMPLOYEE_EDIT_AREA")) {
            String p = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("path");
            Type listType = new TypeToken<List<Point>>() {
            }.getType();
            Gson mapper = new Gson();
            List<Point> path = mapper.fromJson(p, listType);
            int i = 1;
            for (Point po : path) {
                po.setEmployee(selected);
                po.setPriority(i);
                i++;
            }
            selected.setPoints(path);
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    private boolean allShape = false;

    public void allShape() {
        if (currentUser.hasRole("EMPLOYEE_EDIT_AREA")) {
            if (allShape) {
                StringBuilder out = new StringBuilder();
                int i = 0;
                for (Employee e : getItems()) {
                    if (e.getId() != selected.getId()) {
                        if (i != 0) {
                            out.append(",");
                        }
                        out.append(e.jsonPath());
                        i++;
                    }
                }
                Ajax.data("shapes", "[" + out + "]");
            } else {
                Ajax.data("shapes", "[]");
            }
            Ajax.oncomplete("showAllShapes()");
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void removePoints() {
        if (currentUser.hasRole("EMPLOYEE_EDIT_AREA")) {
            selected.setPoints(null);
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public boolean isAllShape() {
        return allShape;
    }

    public void setAllShape() {
        this.allShape = !this.allShape;
    }

    public void setDocValue(String docValue) {
        this.docValue = docValue;
    }

    public String getDocKey() {
        return docKey;
    }

    public void setDocKey(String docKey) {
        this.docKey = docKey;
    }

    public void descriptionSave() {
        if (currentUser.hasRole("EMPLOYEE_EDIT")) {
            selected.getDocuments().put(docKey, docValue);
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public List<Part> getFiles() {
        return files;
    }

    public void setFiles(List<Part> files) {
        this.files = files;
    }

    public List<String> getMarktingRoles() {
        return marktingRoles;
    }

    public void setMarktingRoles(List<String> marktingRoles) {
        this.marktingRoles = marktingRoles;
    }

    public UIData getComponent() {
        return component;
    }

    public void setComponent(UIData component) {
        this.component = component;
    }

    public Part getImage() {
        return image;
    }

    public void setImage(Part image) {
        this.image = image;
    }

    public Employee getSelected() {
        return selected;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public void setSelected(Employee selected) {
        this.selected = selected;
        if (selected != null && this.selected.getRoles() != null) {
            marktingRoles = new ArrayList<>();
            selected.getRoles().keySet().forEach((key) -> {
                marktingRoles.add(key);
            });
        }
    }

    public void saveRoles() {
        if (currentUser.hasRole("EMPLOYEE_CHANGE_ACCESS")) {
            selected.setRoles(new HashMap<>());
            marktingRoles.forEach((s) -> {
                selected.getRoles().put(s, selected.getPosition().getRoles().get(s));
            });
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EmployeeFacade getFacade() {
        return ejbFacade;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Employee prepareCreate() {
        selected = new Employee();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        if (currentUser.hasRole("EMPLOYEE_ADD")) {
            selected.setPlainPassword(selected.getMobile());
            selected.setRoles(selected.getPosition().getRoles());
            persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeCreated"));
            if (!JsfUtil.isValidationFailed()) {
                items = null;    // Invalidate list of items to trigger re-query.
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void changePassword() {
        if (currentUser.hasRole("EMPLOYEE_CHANGE_PASSWORD")) {
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void update() {
        if (currentUser.hasRole("EMPLOYEE_EDIT")) {
            if (province != null) {
                selected.setProvince(province.getName());
            }
            persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("EmployeeUpdated"));
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public void destroy() {
        if (currentUser.hasRole("EMPLOYEE_REMOVE")) {
            persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("EmployeeDeleted"));
            if (!JsfUtil.isValidationFailed()) {
                selected = null; // Remove selection
                items = null;    // Invalidate list of items to trigger re-query.
            }
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
        }
    }

    public List<Employee> getItems() {
        if (currentUser.hasRole("EMPLOYEE_SHOW")) {
            if (items == null) {
                items = getFacade().findAll();
            }
            return items;
        } else {
            FacesMessage message = new FacesMessage("شما مجوز این فعالیت را ندارید!");
            RequestContext.getCurrentInstance().showMessageInDialog(message);
            return null;
        }
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Employee getEmployee(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Employee> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Employee> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Employee.class)
    public static class EmployeeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EmployeeController controller = (EmployeeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "employeeController");
            return controller.getEmployee(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Employee) {
                Employee o = (Employee) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Employee.class.getName()});
                return null;
            }
        }

    }
}
