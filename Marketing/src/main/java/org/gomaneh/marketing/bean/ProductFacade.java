/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Category;
import org.gomaneh.marketing.model.Product;

/**
 *
 * @author davood
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductFacade() {
        super(Product.class);
    }

    public List<String> findFeatureString(String query) {
        return em.createNativeQuery("SELECT f.KEY FROM PRODUCT_FEATURES f  GROUP BY f.KEY").setParameter("query", query).getResultList();
    }

    public List<Product> findByCategory(Category category) {
        if (category == null) {
            return this.findAll();
        } else {
            return em.createQuery("SELECT p FROM Product p WHERE p.category = :category").setParameter("category", category).getResultList();
        }
    }
}
