if (typeof google === 'object' && typeof google.maps === 'object') {
    initMap();
} else {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAcSs4fEMdrLrauggKfRbRqmgM86Wt1OE4&libraries=places,drawing&language=fa&callback=initMap";
    document.body.appendChild(script);
}
function saveShape() {
    if (shape) {
        var vertices = shape.getPath();
        var path = [];
        for (var i = 0; i < vertices.getLength(); i++) {
            var xy = vertices.getAt(i);
            var point = {};
            point['lat'] = xy.lat();
            point['lng'] = xy.lng();
            path.push(point);
        }
        saveShapeInDB({"path": JSON.stringify(path)});
    }
    return false;
}