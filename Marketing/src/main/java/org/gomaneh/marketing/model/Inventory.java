/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "INVENTORY")
@XmlRootElement
public class Inventory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "PRODUCT", referencedColumnName = "ID")
    @ManyToOne
    private Product product;
    @Column(name = "TYPE")
    private short type = 0;
    @Column(name = "NUMBER")
    private int number;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATUS")
    private short status = 0;
    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Lob
    @Column(name = "COMMENT")
    private String comment;
    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "REAL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date realDate;
    @Column(name = "DOC")
    private String doc;
    @Column(name = "DOC_NUMBER")
    private Integer docNumber;
    @JoinColumn(name = "EMPLOYEE", referencedColumnName = "ID")
    @ManyToOne
    private Employee employee;
    @JoinColumn(name = "RECEIVER", referencedColumnName = "ID")
    @ManyToOne
    private Employee receiver;
    @Column(name = "RECEIVER_NAME")
    private String receiverName;
    @Column(name = "RESOLVE")
    private boolean resolve;
    @Column(name = "YEARS")
    private String years;

    public Inventory() {
    }

    public Inventory(Product product, int number, String comment, Date realDate, Employee employee, boolean resolve, String years,Integer docNumber,short type) {
        this.product = product;
        this.number = number;
        this.comment = comment;
        this.realDate = realDate;
        this.employee = employee;
        this.resolve = resolve;
        this.years = years;
        this.docNumber=docNumber;
        this.type=type;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Employee getReceiver() {
        return receiver;
    }

    public void setReceiver(Employee receiver) {
        this.receiver = receiver;
    }

    public Inventory(Integer id) {
        this.id = id;
    }

    public Inventory(Integer id, short type, int number, short status, Date createdAt, String comment, Date updatedAt, Date realDate, String doc, Integer docNumber) {
        this.id = id;
        this.type = type;
        this.number = number;
        this.status = status;
        this.createdAt = createdAt;
        this.comment = comment;
        this.updatedAt = updatedAt;
        this.realDate = realDate;
        this.doc = doc;
        this.docNumber = docNumber;
    }

    public boolean isResolve() {
        return resolve;
    }

    public void setResolve(boolean resolve) {
        this.resolve = resolve;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getRealDate() {
        return realDate;
    }

    public void setRealDate(Date realDate) {
        this.realDate = realDate;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Integer getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(Integer docNumber) {
        this.docNumber = docNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventory)) {
            return false;
        }
        Inventory other = (Inventory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.Inventory[ id=" + id + " ]";
    }

    @PrePersist
    public void prePersist() {
        createdAt = new Date();
    }
}
