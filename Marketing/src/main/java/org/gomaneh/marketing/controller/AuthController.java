/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.controller;

import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.gomaneh.marketing.bean.EmployeeFacade;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.model.Employee;

/**
 *
 * @author davood
 */
@Named(value = "authController")
@org.omnifaces.cdi.ViewScoped
public class AuthController implements Serializable {

    @EJB
    private EmployeeFacade employeeFacade;

    private String username;
    private String password;

    public AuthController() {
    }

    public String login() {
        Employee user = employeeFacade.findByUsername(username);
        if (user==null || !user.checkPassword(password) || user.getStatus() == -1) {
            JsfUtil.addErrorMessage("نام کاربری یا رمز عبور نادرست است.");
            return null;
        }
        if (user.getStatus() == 1) {
            JsfUtil.addErrorMessage("حساب کاربری شما غیر فعال شده است.");
            return null;
        }
        FacesContext context=FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("user", user);
        JsfUtil.addSuccessMessage("به سامانه بازار یابی گمانه خوش آمدید.");
        return "marketing/marketing.xhtml?faces-redirect=true";
    }

    public void logout() throws IOException{
        FacesContext context=FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
        context.getExternalContext().redirect("login.xhtml");
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
