/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "BASEATTRIBUTE_MARKRTTYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BaseattributeMarkrttype.findAll", query = "SELECT b FROM BaseattributeMarkrttype b")
    , @NamedQuery(name = "BaseattributeMarkrttype.findById", query = "SELECT b FROM BaseattributeMarkrttype b WHERE b.id = :id")
    , @NamedQuery(name = "BaseattributeMarkrttype.findByRequired", query = "SELECT b FROM BaseattributeMarkrttype b WHERE b.required = :required")
    , @NamedQuery(name = "BaseattributeMarkrttype.findByMultiple", query = "SELECT b FROM BaseattributeMarkrttype b WHERE b.multiple = :multiple")
    , @NamedQuery(name = "BaseattributeMarkrttype.findByDatatype", query = "SELECT b FROM BaseattributeMarkrttype b WHERE b.datatype = :datatype")
    , @NamedQuery(name = "BaseattributeMarkrttype.findByPriorty", query = "SELECT b FROM BaseattributeMarkrttype b WHERE b.priorty = :priorty")})
public class BaseattributeMarkrttype implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REQUIRED")
    private boolean required=true;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MULTIPLE")
    private boolean multiple=false;
    @Column(name = "DATATYPE")
    private Integer datatype=0;
    @Column(name = "PRIORTY")
    private Integer priorty;
    @JoinColumn(name = "BASEATTRIBUTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Baseattribute baseattribute;
    @JoinColumn(name = "MARKRTTYPE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Markettype markrttype;
    @Column(name = "FEATUREGROUP")
    private int group;
    @Column(name="meta")
    private String meta;

    public BaseattributeMarkrttype() {
    }

    public String getMeta() {
        return meta;
    }

     public List<String> getSelectItems(){
        if(!meta.isEmpty()){
            return Arrays.asList(meta.split(","));
        }
        return new ArrayList();
    }
     
    public void setMeta(String meta) {
        this.meta = meta;
    }

    public BaseattributeMarkrttype(Integer id) {
        this.id = id;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public BaseattributeMarkrttype(Integer id, boolean required, boolean multiple) {
        this.id = id;
        this.required = required;
        this.multiple = multiple;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean getMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public Integer getDatatype() {
        return datatype;
    }

    public void setDatatype(Integer datatype) {
        this.datatype = datatype;
    }

    public Integer getPriorty() {
        return priorty;
    }

    public void setPriorty(Integer priorty) {
        this.priorty = priorty;
    }

    public Baseattribute getBaseattribute() {
        return baseattribute;
    }

    public void setBaseattribute(Baseattribute baseattribute) {
        this.baseattribute = baseattribute;
    }

    public Markettype getMarkrttype() {
        return markrttype;
    }

    public void setMarkrttype(Markettype markrttype) {
        this.markrttype = markrttype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BaseattributeMarkrttype)) {
            return false;
        }
        BaseattributeMarkrttype other = (BaseattributeMarkrttype) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.BaseattributeMarkrttype[ id=" + id + " ]";
    }
    
}
