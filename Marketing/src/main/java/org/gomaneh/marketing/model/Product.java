/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "PRODUCT")
@XmlRootElement
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 1024)
    @Column(name = "NAME")
    private String name;
    @Column(name = "PRICE")
    private Integer price;
    @Size(max = 1024)
    @Column(name = "META")
    private String meta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPLOYEE")
    private int employee;
    @Column(name = "UPDATEAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateat;
    @Column(name = "STATUS")
    private Integer status = 0;
    @Size(max = 128)
    @Column(name = "INVENTORY")
    private Integer inventory;
    @Column(name = "PRIORITY")
    private Integer priority = 1;
    @Column(name = "PARTNUMBER")
    private Integer partnumber;
    @Column(name = "DISCOUNT")
    private Integer discount;
    @Column(name = "LIMIT_ALERT")
    private Integer limit;
    @Lob
    @Size(max = 65535)
    @Column(name = "IMAGES")
    private String images;
    @Column(name = "WEIGHT")
    private Integer weight;
    @Column(name = "LICENSE_NUMBER")
    private String licenseNumber;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 1024)
    @Column(name = "CATALOG")
    private String catalog;
    @Column(name = "VIDEO_MP4")
    private String videoMp4;
    @Column(name = "VIDEO_WEBM")
    private String videoWebm;
    @JoinColumn(name = "CATEGORY", referencedColumnName = "ID")
    @ManyToOne
    private Category category;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProductFeature> features = new ArrayList<>();
    @Transient
    private ProductFeature tempFeature = new ProductFeature(this);
    @Transient
    private List<String> imgs;

    public List<String> getImgs() {
        if (imgs == null) {
            imgs = new ArrayList<>();
            if (images != null && !images.isEmpty()) {
                String[] str = images.split(",");
                if (str.length > 1) {
                    imgs.addAll(Arrays.asList(str));
                } else {
                    imgs.add(images);
                }
            }
        }
        return imgs;
    }

    public List<ProductFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<ProductFeature> features) {
        this.features = features;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public ProductFeature getTempFeature() {
        return tempFeature;
    }

    public void setTempFeature(ProductFeature tempFeature) {
        this.tempFeature = tempFeature;
    }

    public void imageListToString() {
        if (imgs != null) {
            String out = "";
            for (String s : imgs) {
                out += "," + s;
            }
            images = out.substring(1);
        }
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public boolean inventoryAlert() {
        return inventory < limit && inventory > 0;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Product() {
    }

    public Product(Integer id) {
        this.id = id;
    }

    public Product(Integer id, int employee) {
        this.id = id;
        this.employee = employee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public Date getUpdateat() {
        return updateat;
    }

    public void setUpdateat(Date updateat) {
        this.updateat = updateat;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getInventory() {
        return inventory;
    }

    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getPartnumber() {
        return partnumber;
    }

    public void setPartnumber(Integer partnumber) {
        this.partnumber = partnumber;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getVideoMp4() {
        return videoMp4;
    }

    public void setVideoMp4(String videoMp4) {
        this.videoMp4 = videoMp4;
    }

    public String getVideoWebm() {
        return videoWebm;
    }

    public void setVideoWebm(String videoWebm) {
        this.videoWebm = videoWebm;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.bean.Product[ id=" + id + " ]";
    }
}
