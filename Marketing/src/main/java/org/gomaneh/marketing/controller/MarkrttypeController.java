package org.gomaneh.marketing.controller;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.gomaneh.marketing.model.Markettype;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.controller.util.JsfUtil.PersistAction;
import org.gomaneh.marketing.bean.MarkettypeFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import org.gomaneh.marketing.bean.BaseattributeFacade;
import org.gomaneh.marketing.model.Baseattribute;
import org.gomaneh.marketing.model.BaseattributeMarkrttype;
import org.gomaneh.marketing.model.Employee;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Servlets;
import org.primefaces.model.DualListModel;

@Named("markrttypeController")
@ViewScoped
public class MarkrttypeController implements Serializable {

    @EJB
    private org.gomaneh.marketing.bean.MarkettypeFacade ejbFacade;
    @EJB
    private BaseattributeFacade baseAttrFacade;
    private List<Markettype> items = null;
    private Markettype selected;
    private String page = "List";
    private DualListModel<Baseattribute> l1;
    private DualListModel<Baseattribute> l2;
    private DualListModel<Baseattribute> l3;
    private List<BaseattributeMarkrttype> temp;
    private Part part;
    private int groupSection = 0;
    private Employee currentUser;

    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
    }

    public Employee getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Employee currentUser) {
        this.currentUser = currentUser;
    }

    public MarkrttypeController() {
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public void upload() throws IOException {
        if (part != null) {
            String relativeWebPath = "/resources/img/ban.png";
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            ServletContext servletContext = (ServletContext) externalContext.getContext();
            String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);
            Image ban = ImageIO.read(new File(absoluteDiskPath));
            String home = System.getProperty("user.home");
            File dir = new File(home + "/media");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String name = Servlets.getSubmittedFileName(part);
            Image img = ImageIO.read(part.getInputStream());
            BufferedImage tmp = new BufferedImage(36, 36, BufferedImage.TYPE_INT_ARGB);
            BufferedImage tmp2 = new BufferedImage(36, 36, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) tmp.getGraphics();
            g.drawImage(img, 6, 6, 24, 24, null);
            g.setComposite(AlphaComposite.Src);
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Graphics2D g2 = (Graphics2D) tmp2.getGraphics();
            g2.drawImage(img, 6, 6, 24, 24, null);
            g2.drawImage(ban, 6, 6, 24, 24, null);
            g2.setComposite(AlphaComposite.Src);
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            ImageIO.write(tmp2, "PNG", new FileOutputStream(home + "/media/Ban" + name));
            ImageIO.write(tmp, "PNG", new FileOutputStream(home + "/media/L1" + name));
            g.drawOval(4, 4, 28, 28);
            ImageIO.write(tmp, "PNG", new FileOutputStream(home + "/media/L3" + name));
            g.setColor(Color.RED);
            g.drawOval(1, 1, 34, 34);
            ImageIO.write(tmp, "PNG", new FileOutputStream(home + "/media/L2" + name));
            g.drawOval(4, 4, 28, 28);
            ImageIO.write(tmp, "PNG", new FileOutputStream(home + "/media/L3" + name));
            selected.setIcon(name);
            update();
        }
    }

    public Markettype getSelected() {
        return selected;
    }

    public void setSelected(Markettype selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    public String getPage() {
        return page;
    }

    public int getGroupSection() {
        return groupSection;
    }

    public void setGroupSection(int groupSection) {
        this.groupSection = groupSection;
    }

    public void setPage(String page) {
        this.page = page;
    }

    private MarkettypeFacade getFacade() {
        return ejbFacade;
    }

    public void prepareUpdate() {
        List<Baseattribute> src1 = new ArrayList<>(baseAttrFacade.findAll());
        List<Baseattribute> src2 = new ArrayList<>(src1);
        List<Baseattribute> src3 = new ArrayList<>(src1);
        List<Baseattribute> dest1 = new ArrayList<>();
        List<Baseattribute> dest2 = new ArrayList<>();
        List<Baseattribute> dest3 = new ArrayList<>();
        for (BaseattributeMarkrttype a : selected.getBaseattributeMarkrttypeCollection()) {
            switch (a.getGroup()) {
                case 1:
                    src1.remove(a.getBaseattribute());
                    dest1.add(a.getBaseattribute());
                    break;
                case 2:
                    src2.remove(a.getBaseattribute());
                    dest2.add(a.getBaseattribute());
                    break;
                default:
                    src3.remove(a.getBaseattribute());
                    dest3.add(a.getBaseattribute());
                    break;
            }
        }
        l1 = new DualListModel<>(src1, dest1);
        l2 = new DualListModel<>(src2, dest2);
        l3 = new DualListModel<>(src3, dest3);
        page = "Create";
    }

    public Markettype prepareCreate() {
        selected = new Markettype();
        List<Baseattribute> src = new ArrayList<>(baseAttrFacade.findAll());
        List<Baseattribute> dest1 = new ArrayList<>();
        List<Baseattribute> dest2 = new ArrayList<>();
        List<Baseattribute> dest3 = new ArrayList<>();
        l1 = new DualListModel<>(src, dest1);
        l2 = new DualListModel<>(src, dest2);
        l3 = new DualListModel<>(src, dest3);
        page = "Create";
        return selected;
    }

    public void preCreate() {
        temp = new ArrayList();
        setAttr(l1.getTarget(), 1);
        setAttr(l2.getTarget(), 2);
        setAttr(l3.getTarget(), 3);
        selected.setBaseattributeMarkrttypeCollection(temp);
        page = "InputType";
    }

    private void setAttr(List<Baseattribute> l, int group) {
        BaseattributeMarkrttype attr;
        int i = 0;
        for (Baseattribute a : l) {
            attr = null;
            attr = existBaseAttr(a, group);
            if (attr == null) {
                attr = new BaseattributeMarkrttype();
                attr.setMarkrttype(selected);
                attr.setBaseattribute(a);
                attr.setPriorty(i);
                attr.setGroup(group);
            }
            temp.add(attr);
            i++;
        }
    }

    private BaseattributeMarkrttype existBaseAttr(Baseattribute attr, int g) {
        for (BaseattributeMarkrttype a : selected.getBaseattributeMarkrttypeCollection()) {
            if (a.getGroup() == g && attr.getId() == a.getBaseattribute().getId()) {
                return a;
            }
        }
        return null;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("MarkrttypeCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
            page = "List";
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("MarkrttypeUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("MarkrttypeDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public DualListModel<Baseattribute> getL1() {
        return l1;
    }

    public void setL1(DualListModel<Baseattribute> l1) {
        this.l1 = l1;
    }

    public DualListModel<Baseattribute> getL2() {
        return l2;
    }

    public void setL2(DualListModel<Baseattribute> l2) {
        this.l2 = l2;
    }

    public DualListModel<Baseattribute> getL3() {
        return l3;
    }

    public void setL3(DualListModel<Baseattribute> l3) {
        this.l3 = l3;
    }

    public List<Markettype> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Markettype getMarkrttype(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Markettype> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Markettype> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Markettype.class)
    public static class MarkrttypeControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            MarkrttypeController controller = (MarkrttypeController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "markrttypeController");
            return controller.getMarkrttype(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Markettype) {
                Markettype o = (Markettype) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Markettype.class.getName()});
                return null;
            }
        }

    }

}
