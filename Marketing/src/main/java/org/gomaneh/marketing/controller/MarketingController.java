/**
 * This Class Fro Marketing use
 */
package org.gomaneh.marketing.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.gomaneh.marketing.bean.BaseattributeFacade;
import org.gomaneh.marketing.bean.MarketplaceFacade;
import org.gomaneh.marketing.bean.MarkettypeFacade;
import org.gomaneh.marketing.controller.util.JsfUtil;
import org.gomaneh.marketing.model.Baseattribute;
import org.gomaneh.marketing.model.BaseattributeMarkrttype;
import org.gomaneh.marketing.model.Employee;
import org.gomaneh.marketing.model.Feature;
import org.gomaneh.marketing.model.Marketplace;
import org.gomaneh.marketing.model.Markettype;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Ajax;
import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;

/**
 *
 * @author MrMostafa
 */
@ViewScoped
@Named("marketingController")
public class MarketingController implements Serializable {

    @EJB
    private MarketplaceFacade marketplaceFacade;
    @EJB
    private MarkettypeFacade markettypeFacade;
    @EJB
    private BaseattributeFacade baseAttributeFacade;
    private List<Markettype> marketTypes;
    private List<Baseattribute> baseAttributes;
    private List<Marketplace> marketPlaces;
    private String centerGeoMap = "35.7131889,48.420484";
    private MapModel geoModel;
    private Marketplace marketSelected = new Marketplace();
    private Marketplace selected;
    private String page = "base";
    private final MapModel emptyModel = new DefaultMapModel();
    private List<Feature> baseAttr = new ArrayList<>();
    private List<Feature> placesAttr = new ArrayList<>();
    private List<Feature> competionAttr = new ArrayList<>();
    private Employee currentUser;

    public enum Action {
        ADD, EDIT, SHOW, REMOVE
    }

    @PostConstruct
    public void init() {
        marketTypes = markettypeFacade.findAll();
        FacesContext context = FacesContext.getCurrentInstance();
        currentUser = (Employee) context.getExternalContext().getSessionMap().get("user");
    }

    public Employee getCurrentUser() {
        return currentUser;
    }

    private String title;

    public MapModel getEmptyModel() {
        if (emptyModel != null) {

        }
        return emptyModel;
    }

    private boolean fullPiont = false;

    public void callMarkers() {
        String lat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectLat");
        String lng = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectLng");
        marketPlaces = marketplaceFacade.findByPositions(fullPiont, lat, lng, currentUser, 5);
        StringBuilder out = new StringBuilder();
        int i = 0;
        for (Marketplace m : marketPlaces) {
            if (i != 0) {
                out.append(",");
            }
            String icon = (Objects.equals(m.getEmployee().getId(), currentUser.getId()) ? (m.getPriorty() == 0 ? "L1" : (m.getPriorty() == 1 ? "L2" : "L3")) : "Ban") + m.getMarkrttype().getIcon();
            out.append(String.format("{\"position\": {\"lat\":%s,\"lng\":%s},\"icon\": \"%s\",\"title\":\"%s\",\"market_type\":\"%s\"}", m.getLatitude(), m.getLongitude(), icon, m.getName(), m.getMarkrttype().getName()));
            i++;
        }
        Ajax.data("markers", "[" + out + "]");
        Ajax.oncomplete("handleMessage();");
    }

    public void prepareCreate() {
        marketSelected = new Marketplace();
    }

    public void setFullPiont(boolean fullPiont) {
        if (this.fullPiont) {
            this.fullPiont = false;
        } else {
            this.fullPiont = true;
        }
        callMarkers();
    }

    public void onMarkerSelect() {
        String lat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectLat");
        String lng = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectLng");
        marketSelected = marketplaceFacade.findMarketByPositions(lat, lng);
        Ajax.data("market", String.format("{\"title\":\"%s\",\"type\":\"%s\"}", marketSelected.getName(), marketSelected.getMarkrttype().getName()));
        Ajax.oncomplete("marketSelect();");
    }

    public void removeMarket() {
        if (marketSelected != null && isGrant(Action.REMOVE, marketSelected)) {
            marketplaceFacade.remove(marketSelected);
            callMarkers();
        } else {
            JsfUtil.addErrorMessage("مجوز این فعالیت برای شما وجود ندارد.");
        }
    }

    public boolean isGrant(Action action, Marketplace market) {
        switch (action) {
            case EDIT:
                return (currentUser.hasRole("MARKETING_ALL_EDIT") || (currentUser.hasRole("MARKETING_SELF_EDIT") && market.getEmployee() != null && Objects.equals(market.getEmployee().getId(), currentUser.getId())));
            case SHOW:
                return (currentUser.hasRole("MARKETING_ALL_SHOW") || (currentUser.hasRole("MARKETING_SELF_SHOW") && market.getEmployee() != null  && Objects.equals(market.getEmployee().getId(), currentUser.getId())));
            case REMOVE:
                return (currentUser.hasRole("MARKETING_ALL_REMOVE")
                        || (currentUser.hasRole("MARKETING_SELF_REMOVE") && market.getEmployee() != null 
                        && Objects.equals(market.getEmployee().getId(),
                                currentUser.getId())));
        }
        return false;
    }

    public void preparEdit() {

        if (marketSelected.getId() != null) {
            baseAttr.clear();
            placesAttr.clear();
            competionAttr.clear();
            for (Feature feature : marketSelected.getFeatures()) {
                switch (feature.getType().getGroup()) {
                    case 1:
                        baseAttr.add(feature);
                        break;
                    case 2:
                        placesAttr.add(feature);
                        break;
                    default:
                        competionAttr.add(feature);
                        break;
                }
            }
            Ajax.update("formsection");
            Ajax.oncomplete("$('.mymodal').show();");
        }
    }

    public String getTitle() {
        return title;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFullPiont() {
        return fullPiont;
    }

    public List<Marketplace> getMarketPlaces() {
        return marketPlaces;
    }

    public void setMarketPlaces(List<Marketplace> marketPlaces) {
        this.marketPlaces = marketPlaces;
    }

    private Markettype marketType;
    private Marketplace marketPlace = new Marketplace();

    public Marketplace getMarketPlace() {
        return marketPlace;
    }

    public void setMarketPlace(Marketplace marketPlace) {
        this.marketPlace = marketPlace;
    }

    public Markettype getMarketType() {
        return marketType;
    }

    public void setMarketType(Markettype marketType) {
        this.marketType = marketType;
    }

    public void changeMarketType() {
        if (marketSelected.getMarkrttype() != null) {
            baseAttr.clear();
            placesAttr.clear();
            competionAttr.clear();
            for (BaseattributeMarkrttype b : marketSelected.getBaseAttr()) {
                Feature f = new Feature();
                f.getValueList().add(null);
                f.setType(b);
                f.setPriorty(b.getPriorty());
                f.setFeature(b.getBaseattribute().getName());
                baseAttr.add(f);
            }
            for (BaseattributeMarkrttype b : marketSelected.getPlacesAttr()) {
                Feature f = new Feature();
                f.getValueList().add(null);
                f.setType(b);
                f.setPriorty(b.getPriorty());
                f.setFeature(b.getBaseattribute().getName());
                placesAttr.add(f);
            }
            for (BaseattributeMarkrttype b : marketSelected.getCompetitionAttr()) {
                Feature f = new Feature();
                f.getValueList().add(null);
                f.setType(b);
                f.setPriorty(b.getPriorty());
                f.setFeature(b.getBaseattribute().getName());
                competionAttr.add(f);
            }
        }
    }

    public void plusAtr(int group, int id) {
        if (group == 1) {
            baseAttr.get(id).getValueList().add(null);
        } else if (group == 2) {
            placesAttr.get(id).getValueList().add(null);
        } else {
            competionAttr.get(id).getValueList().add(null);
        }
    }

    public void removeAttr(int group, int id, int idinput) {
        if (group == 1) {
            baseAttr.get(id).getValueList().remove(idinput);
        } else if (group == 2) {
            placesAttr.get(id).getValueList().remove(idinput);
        } else {
            competionAttr.get(id).getValueList().remove(idinput);
        }
    }

    public void submit() {
        if (marketSelected != null
                && ((currentUser.hasRole("MARKETING_ALL_ADD") || (currentUser.hasRole("MARKETING_SELF_ADD") && currentUser.pointInPolygon(marketSelected.getLatitude(), marketSelected.getLongitude()))))
                || isGrant(Action.EDIT, marketSelected)) {
            Marketplace m = new Marketplace();
            List<Feature> feature = new ArrayList<>(baseAttr);
            feature.addAll(placesAttr);
            feature.addAll(competionAttr);
            feature.stream().forEach((f) -> {
                f.setMarketplace(m);
            });
            m.setId(marketSelected.getId());
            m.setFeatures(feature);
            m.setAddress(marketSelected.getAddress());
            m.setCity(marketSelected.getCity());
            m.setPriorty(marketSelected.getPriorty());
            m.setLatitude(marketSelected.getLatitude());
            m.setLongitude(marketSelected.getLongitude());
            m.setProvince(marketSelected.getProvince());
            m.setMarkrttype(marketSelected.getMarkrttype());
            m.setName(marketSelected.getName());
            m.setEmployee(currentUser);
            marketplaceFacade.edit(m);
            marketPlaces.add(m);
            placesAttr.clear();
            competionAttr.clear();
            page = "base";
            baseAttr.clear();
            String icon = (m.getPriorty() == 0 ? "L1" : (m.getPriorty() == 1 ? "L2" : "L3")) + m.getMarkrttype().getIcon();
            Ajax.oncomplete("finishSubmit('" + String.format("{\"position\": {\"lat\":%s,\"lng\":%s},\"icon\": \"%s\",\"title\":\"%s\",\"market_type\":\"%s\"}", m.getLatitude(), m.getLongitude(), icon, m.getName(), m.getMarkrttype().getName()) + "');");
            marketSelected = new Marketplace();
            marketType = null;
            JsfUtil.addSuccessMessage("مکان جدید افزوده شد.");
        } else {
            JsfUtil.addErrorMessage("مجوز این فعالیت برای شما وجود ندارد.");
        }
    }

    public List<Feature> getBaseAttr() {
        return baseAttr;
    }

    public void setBaseAttr(List<Feature> baseAttr) {
        this.baseAttr = baseAttr;
    }

    public List<Feature> getPlacesAttr() {
        return placesAttr;
    }

    public void setPlacesAttr(List<Feature> placesAttr) {
        this.placesAttr = placesAttr;
    }

    public List<Feature> getCompetionAttr() {
        return competionAttr;
    }

    public void setCompetionAttr(List<Feature> competionAttr) {
        this.competionAttr = competionAttr;
    }

    public List<Markettype> getMarketTypes() {
        return marketTypes;
    }

    public void setMarketTypes(List<Markettype> marketTypes) {
        this.marketTypes = marketTypes;
    }

    public void onGeocode(GeocodeEvent event) {
        List<GeocodeResult> results = event.getResults();
        if (results != null && !results.isEmpty()) {
            LatLng center = results.get(0).getLatLng();
            System.out.println(center.getLat() + "," + center.getLng());
            centerGeoMap = center.getLat() + "," + center.getLng();
        }
    }

    public MapModel getGeoModel() {
        return geoModel;
    }

    public void setGeoModel(MapModel geoModel) {
        this.geoModel = geoModel;
    }

    public String getCenterGeoMap() {
        return centerGeoMap;
    }

    public void setCenterGeoMap(String centerGeoMap) {
        this.centerGeoMap = centerGeoMap;
    }

    public Marketplace getMarketSelected() {
        return marketSelected;
    }

    public void setMarketSelected(Marketplace marketSelected) {
        this.marketSelected = marketSelected;
    }
}
