/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.bean;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.gomaneh.marketing.model.Province;

/**
 *
 * @author davood
 */
@Stateless
public class ProvinceFacade extends AbstractFacade<Province> {

    @PersistenceContext(unitName = "org.gomaneh_Marketing_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProvinceFacade() {
        super(Province.class);
    }

    public Province findByName(String name) {
        try {
            return (Province) em.createQuery("SELECT p FROM Province p WHERE p.name = :name").setParameter("name", name).setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
