/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gomaneh.marketing.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author davood
 */
@Entity
@Table(name = "ORDERITEMS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orderitems.findAll", query = "SELECT o FROM Orderitems o")
    , @NamedQuery(name = "Orderitems.findById", query = "SELECT o FROM Orderitems o WHERE o.id = :id")
    , @NamedQuery(name = "Orderitems.findByNumberorder", query = "SELECT o FROM Orderitems o WHERE o.numberorder = :numberorder")
    , @NamedQuery(name = "Orderitems.findByStatus", query = "SELECT o FROM Orderitems o WHERE o.status = :status")
    , @NamedQuery(name = "Orderitems.findByMeta", query = "SELECT o FROM Orderitems o WHERE o.meta = :meta")})
public class Orderitems implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NUMBERORDER")
    private Integer numberorder;
    @Column(name = "STATUS")
    private Integer status;
    @Size(max = 1024)
    @Column(name = "META")
    private String meta;
    @JoinColumn(name = "PRODUCT", referencedColumnName = "ID")
    @ManyToOne
    private Product product;
    @JoinColumn(name = "ORDERPRODUCT", referencedColumnName = "ID")
    @ManyToOne
    private Orderproduct orderproduct;

    public Orderitems() {
    }

    public Orderitems(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberorder() {
        return numberorder;
    }

    public void setNumberorder(Integer numberorder) {
        this.numberorder = numberorder;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Orderproduct getOrderproduct() {
        return orderproduct;
    }

    public void setOrderproduct(Orderproduct orderproduct) {
        this.orderproduct = orderproduct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orderitems)) {
            return false;
        }
        Orderitems other = (Orderitems) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.gomaneh.marketing.model.Orderitems[ id=" + id + " ]";
    }
    
}
